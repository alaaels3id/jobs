@extends('backend.layouts.app')

@section('title', 'Deleted Jops')

@section('styles')
	<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@stop

@section('content')
    
    <section class="content-header">
        <h1>Dashboard<small>Control panel</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Trashed</li>
        </ol>
    </section>

    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box">
    				<div class="box-header">
    					<h3 class="box-title">Data Table With Full Features</h3>
    				</div>
    				<!-- /.box-header -->
    				<div class="box-body">
    					<table id="jops" class="table table-bordered table-striped text-center">
    						
    						<thead>
    							<tr>
    								<th>#</th>
				                  	<th>Name</th>
				                  	<th>Salary</th>
				                  	<th>Description</th>
				                  	<th>Actions</th>
				                </tr>
	                		</thead>
	                		
	                		<tbody data-count={{ $jops->count() }}>
			                	@foreach($jops as $key => $jop)
				                <tr id="jop-row-{{ $jop->id }}">
				                	<td>{{ (int)$key+1 }}</td>
				                	<td>{{ ucwords($jop->name) }}</td>
				                	<td>{{ $jop->salary }}</td>
				                	<td>{{ ucwords($jop->description) }}</td>
				                	
				                	<td>
				                		<ul class="list-unstyled list-inline">
				                			<li>
				                				<a 
				                					href="{{ url('/jops/'.$jop->id) }}" 

				                					data-toggle='tooltip' title="Restore"

				                					data-id="{{ $jop->id }}" 

				                					class="btn btn-primary btn-sm restore-action">
				                					
				                					<i class="fa fa-repeat"></i>
				                				</a>
				                			</li>

				                			<li>
				                				<a 
				                					href='{{ url('/jops/'.$jop->id) }}' 
				                					
				                					class='btn btn-sm btn-danger delete-action' 
				                					
				                					type='button' data-id="{{ $jop->id }}"

				                					data-toggle='tooltip' title='Delete'>

				                					<i class='fa fa-times'></i>
												</a>
				                			</li>
				                		</ul>
				                	</td>
				                </tr>
			                	@endforeach
	                		</tbody>
	                		
	                		<tfoot>
	                			<tr>
    								<th>#</th>
				                  	<th>Name</th>
				                  	<th>Salary</th>
				                  	<th>Description</th>
				                  	<th>Actions</th>
	                			</tr>
	                		</tfoot>
	              		
	              		</table>
	            	</div>
	            	<!-- /.box-body -->
	          	</div>
            </div>
          </div>
        </div>
      </div>
    </section>

@stop

@section('scripts')
	<script src="{{ asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script>

		$('.delete-action').on('click', function(e)
		{
			var id = $(this).data('id');
			e.preventDefault();
	        
	        bootbox.confirm({
	            title: 'Confirmation Message',
	            size: "small",
	            message: 'Are you sure you want to remove this jop',
	            buttons: {
	                cancel: {
	                    label: '<i class="fa fa-times"></i> Cancel'
	                },
	                confirm: {
	                    label: '<i class="fa fa-check"></i> Confirm',
	                    className: "btn-danger"
	                }
	            },
	            callback: function(result) 
	            {
	                if (result) 
	                {
						$.ajax({
							type: 'POST',
							url: '/admin-panel/jops/all/ajax-remove-jop',
							data: {id: id},
							success: function(response){ $('#jop-row-'+id).fadeOut(); },
							error: function(x) { crud_handle_server_errors(x); },
							complete: function() {
								// var tbody = $('table#jops tbody');
								// var count = tbody.data('count');
								// if(count == 1) tbody.append(`<tr><td colspan="5"><strong>No Trashed</strong></td></tr>`);
							}
						});
	                }
	            }
	        });
		});

		$('.restore-action').on('click', function(e)
		{
			var id = $(this).data('id');
			e.preventDefault();
	        
	        bootbox.confirm({
	            title: 'Confirmation Message',
	            size: "small",
	            message: 'Are you sure you want to restore this jop',
	            buttons: {
	                cancel: {
	                    label: '<i class="fa fa-times"></i> Cancel'
	                },
	                confirm: {
	                    label: '<i class="fa fa-check"></i> Restore',
	                    className: "btn-primary"
	                }
	            },
	            callback: function(result) 
	            {
	                if (result) 
	                {
						$.ajax({
							type: 'POST',
							url: '/admin-panel/jops/all/ajax-restore-jop',
							data: {id: id},
							success: function(response){ $('#jop-row-'+id).fadeOut(); },
							error: function(x) { crud_handle_server_errors(x); },
							complete: function() { 
								// var tbody = $('table#jops tbody');
								// var count = tbody.data('count');
								// if(count == 1) tbody.append(`<tr><td colspan="5"><strong>No Trashed</strong></td></tr>`);	
							}
						});
	                }
	            }
	        });
		});

		$(function () {
			$('#jops').DataTable({
	      		'paging'      : true,
	      		'lengthChange': true,
	      		'searching'   : true,
			    'ordering'    : true,
			    'info'        : true,
			    'autoWidth'   : true
	    	})
	  	});

	</script>
@stop