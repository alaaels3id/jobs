<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ getImage('admin', $auth->image) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ ucwords($auth->name) }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
      
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">

            @if(Auth::user()->guard == 'admin')

                <li class="header">ADMIN MAIN NAVIGATION</li>

                @foreach(models() as $model)
                    @if($model[0] != 'contact')
                    <li class="{{ Request::is('admin-panel/'.str()->plural($model[0]).'/*') ? ' active ' : '' }} {{ Request::is('admin-panel/'.str()->plural($model[0])) ? 'active' : '' }} treeview">
                        <a href="#">
                            <i class="ion {{ $model[1] }}"></i>
                            <span>{{ ucfirst(str()->plural($model[0])) }}</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{{ Request::is('admin-panel/'.str()->plural($model[0])) ? 'active' : '' }}">
                                <a href="{{ route(str()->plural($model[0]).'.index') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span class="pull-right-container">
                                        <small class="label pull-right bg-blue">{{ getModelCount($model[0]) }}</small>
                                    </span> All
                                </a>
                            </li>
                            <li class="{{ Request::is('admin-panel/'.str()->plural($model[0]).'/create') ? 'active' : '' }}">
                                <a href="{{ route(str()->plural($model[0]).'.create') }}"><i class="fa fa-circle-o"></i> Add</a>
                            </li>
                            <li class="{{ Request::is('admin-panel/'.str()->plural($model[0]).'/all/trashed') ? 'active' : '' }}">
                                <a href="{{ route(str()->plural($model[0]).'.trashed') }}">
                                    <i class="fa fa-circle-o"></i> 
                                    <span class="pull-right-container">
                                        <small class="label pull-right bg-red">{{ getModelCount($model[0], true) }}</small>
                                    </span> Trashed
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                @endforeach

                <li>
                    <a href="{{ route('contacts.index') }}">
                        <i class="fa fa-envelope"></i> <span>Mailbox</span>
                        <span class="pull-right-container">
                            <small class="label pull-right bg-green">{{ contact()->count() }}</small>
                        </span>
                    </a>
                </li>
            @endif
        </ul>
    </section>
</aside>