<div class="form-group">
	@include('includes.backend.inputs', ['errors' => $errors, 'type' => 'text', 'name' => 'name', 'style' => 'form-control form-data', 'col' => '6'])
	@include('includes.backend.inputs', [
		'errors' => $errors, 'type' => 'select', 
		'name' => 'user_id', 'style' => 'form-control form-data',
		'list' => $list['users'], 'col' => '6'
	])
</div>

<div class="form-group">
	@include('includes.backend.inputs', [
		'errors' => $errors, 'type' => 'select', 
		'name' => 'city_id', 'style' => 'form-control form-data',
		'list' => $list['cities'], 'col' => '6'
	])
	@include('includes.backend.inputs', [
		'errors' => $errors, 'type' => 'select', 
		'name' => 'department_id', 'style' => 'form-control form-data',
		'list' => $list['departments'], 'col' => '6'
	])
</div>