<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCompanyRequest;
use App\Http\Requests\EditCompanyRequest;
use App\Models\Company;
use App\Models\Department;
use App\Models\City;
use App\Models\CompanyJop;
use Auth, DB, Image, File, Validator, Response, Hash;

class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::all();
        return view('Backend.companies.index', compact('companies'));
    }

    public function home()
    {
        return view('Backend.companies.home');
    }

    public function Trashed()
    {
        $companies = Company::onlyTrashed()->get();
        return view('Backend.companies.trashed', compact('companies'));
    }

    public function Search(Request $request)
    {
        $request->validate(['search' => 'string']);
        $search = $request->search;
        $results = jop()->where('name', 'LIKE', '%'. $search .'%')->where('status', 1)->get();
        return view('includes.front.search', compact('results'));
    }

    public function Jobs()
    {
        $jobs = company_jop()->where('company_id', auth()->id())->get();
        return view('includes.front.company.jobs', compact('jobs'));
    }

    public function companyProfile($id)
    {
        $company = Company::find($id);
        return view('includes.front.company.profile', compact('company'));
    }

    public function UserProfile($id)
    {
        $user = user()->find($id);
        return view('includes.front.company.student', compact('user'));
    }

    public function GetJobApplyers($id)
    {
        $company = company()->find($id);
        $company_jops = company_jop()->where('company_id', $company->id)->get()->pluck('jop_id')->toArray();
        $appliers = jop_user()->whereIn('jop_id', $company_jops)->get();
        return view('includes.front.company.appliers', compact('appliers'));
    }

    public function AddNewJob(Request $request, $id)
    {
        $companyData = $request->all();
        $companyData['company_id'] = $id;

        if(CompanyJop::createCompanyJop($companyData))
        {
            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Added Successfully')]);
            } else {
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Added Successfully'));
            }
        }
        else
        {
            if(request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Internal Server Error 500')]);
            } else {
                request()->session()->flash('status','danger');
                request()->session()->flash('message',__('Internal Server Error 500'));
            }

            return back()->withInputs();
        }
        return redirect()->route('company-panel');
    }

    public function CompanyUpdateProfile(Request $request, $id)
    {
        $company = Company::find($id);
        $updatedProfile = $company->updateCompany(array_except($request->all(), ['_token', 'submit']), $company);
        if($updatedProfile) return back()->with('success', 'Data Updated Successfully !');
        return back()->withInputs();
    }

    public function create()
    {
        $lists['cities'] = City::getInSelectForm();
        $lists['departments'] = Department::getInSelectForm();
        return view('Backend.companies.create', compact('lists'));
    }

    public function store(CreateCompanyRequest $request)
    {
        $companyData = $request->all();

        $excepts = ['password_confirmation', '_token'];

        if(Company::createCompany(array_except($companyData, $excepts)))
        {
            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Added Successfully')]);
            } else {
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Added Successfully'));
            }
        }
        else
        {
            if(request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Internal Server Error 500')]);
            } else {
                request()->session()->flash('status','danger');
                request()->session()->flash('message',__('Internal Server Error 500'));
            }

            return back()->withInputs();
        }
        return redirect()->route('companies.index');
    }

    public function ChangeCompanyStatus(Request $request)
    {
        // Request ajax from index;
        $companyData = $request->all();
        $currentCompany = Company::find($request->id);
        
        if (!$updateCompanyStatus = Company::updateCompanyStatus($companyData, $currentCompany))
            return response()->json(['requestStatus' => false, 'message' => trans('Server Internal Error 500')]);

        return response()->json(['requestStatus' => true, 'message' => trans('Data Updated Successfully')]);
    }

    public function edit($id)
    {
        $company = Company::find($id);
        $lists['cities'] = City::getInSelectForm();
        $lists['departments'] = Department::getInSelectForm();
        return view('Backend.companies.edit', compact('company', 'lists'));
    }

    public function update(EditCompanyRequest $request, $id)
    {
        $companyData = $request->all();
        $currentCompany = Company::find($id);
        
        if (Company::updateCompany($companyData, $currentCompany) ) {

            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Updated Successfully')]);
            }else{
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Updated Successfully'));
            }
        }else{
            if (request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Server Internal Error 500')]);
            }else{
                 request()->session()->flash('status','danger');
                 request()->session()->flash('message',__('Server Internal Error 500'));
            }
        }

        return redirect()->route('companies.edit', ['id' => $currentCompany->id]);
    }

    public function DeleteCompany(Request $request)
    {
        $company = Company::find($request->id);

        if(!$company) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, Company is not exists !!']);

        try
        {
            if ($company->image) File::delete('uploaded/companies/'.$company->image);
            
            $company->delete();
            
            return response()->json(['deleteStatus' => true, 'message' => 'Company Deleted Successfully']);
        }
        catch (Exception $e){ return response()->json(['deleteStatus' => false,'error' => 'Server Internal Error 500']); }

        return redirect()->route('companies.index');
    }

    public function RestoreCompany(Request $request)
    {
        $restore = DB::table('companies')->where('id', $request->id)->where('deleted_at', '!=', null)->update(['deleted_at' => null]);
        
        $response['requestStatus']  = ($restore) ? true : false;
        $response['message']        = ($restore) ? 'Company Restored Successfully' : 'Server Internal Error 500';

        return response()->json($response);
    }

    public function RemoveCompany(Request $request)
    {
        $remove = DB::table('companies')->where('id', $request->id)->where('deleted_at', '!=', null);

        if (isset($remove->image) && $remove->image != null) File::delete('/uploaded/companies/'.$remove->image);

        $remove->delete();
        
        $response['requestStatus']  = ($remove) ? true : false;

        return response()->json($response);
    }
}
