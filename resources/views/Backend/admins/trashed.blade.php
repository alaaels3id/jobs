@extends('backend.layouts.app')

@section('title', 'Deleted Admins')

@section('styles')
	<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@stop

@section('content')
    
    <section class="content-header">
        <h1>Dashboard<small>Control panel</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Trashed</li>
        </ol>
    </section>

    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box">
    				<div class="box-header">
    					<h3 class="box-title">Data Table With Full Features</h3>
    				</div>
    				<!-- /.box-header -->
    				<div class="box-body">
    					<table id="admins" class="table table-bordered table-striped text-center">
    						
    						<thead>
    							<tr>
    								<th>#</th>
				                  	<th>Name</th>
				                  	<th>Email</th>
				                  	<th>Phone</th>
				                  	<th>Address</th>
				                  	<th>Image</th>
				                  	<th>Actions</th>
				                </tr>
	                		</thead>
	                		
	                		<tbody data-count={{ $admins->count() }}>
			                	@foreach($admins as $key => $admin)
				                <tr id="admin-row-{{ $admin->id }}">
				                	<td>{{ (int)$key+1 }}</td>
				                	<td>{{ ucwords($admin->fullname) }}</td>
				                	<td><a href="mailto:{{ $admin->email }}">{{ $admin->email }}</a></td>
				                	<td>{{ $admin->phone }}</td>
				                	<td>{{ ucwords(str_limit($admin->address, 20, ' (..)')) }}</td>

				                	<td>
				                		<img width="35" height="35" class="img-circle" src="{{ getImage('admin', $admin->image) }}" alt="">
				                	</td>
				                	
				                	<td>
				                		<ul class="list-unstyled list-inline">
				                			<li>
				                				<a 
				                					href="{{ url('/admins/'.$admin->id) }}" 

				                					data-toggle='tooltip' title="Restore"

				                					data-id="{{ $admin->id }}" 

				                					class="btn btn-primary btn-sm restore-action">
				                					
				                					<i class="fa fa-repeat"></i>
				                				</a>
				                			</li>

				                			<li>
				                				<a 
				                					href='{{ url('/admins/'.$admin->id) }}' 
				                					
				                					class='btn btn-sm btn-danger delete-action' 
				                					
				                					type='button' data-id="{{ $admin->id }}"

				                					data-toggle='tooltip' title='Delete'>

				                					<i class='fa fa-times'></i>
												</a>
				                			</li>
				                		</ul>
				                	</td>
				                </tr>
			                	@endforeach
	                		</tbody>
	                		
	                		<tfoot>
	                			<tr>
    								<th>#</th>
				                  	<th>Name</th>
				                  	<th>Email</th>
				                  	<th>Phone</th>
				                  	<th>Address</th>
				                  	<th>Image</th>
				                  	<th>Actions</th>
	                			</tr>
	                		</tfoot>
	              		
	              		</table>
	            	</div>
	            	<!-- /.box-body -->
	          	</div>
            </div>
          </div>
        </div>
      </div>
    </section>

@stop

@section('scripts')
	<script src="{{ asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script>

		$('.delete-action').on('click', function(e)
		{
			var id = $(this).data('id');
			var tbody = $('table#admins tbody');
			var count = tbody.data('count');

			e.preventDefault();
	        
	        bootbox.confirm({
	            title: 'Confirmation Message',
	            size: "small",
	            message: 'Are you sure you want to remove this Admin',
	            buttons: {
	                cancel: { label: '<i class="fa fa-times"></i> Cancel' },
	                confirm: {
	                    label: '<i class="fa fa-check"></i> Confirm',
	                    className: "btn-danger"
	                }
	            },
	            callback: function(result) 
	            {
	                if (result) 
	                {
						$.ajax({
							type: 'POST',
							url: '/admin-panel/admins/all/ajax-remove-admin',
							data: {id: id},
							success: function(response){ $('#admin-row-'+id).fadeOut(); count--;},
							error: function(x) { crud_handle_server_errors(x); },
							complete: function() {
								// if(count == 1) tbody.append(`<tr><td colspan="5"><strong>No Trashed</strong></td></tr>`);
							}
						});
	                }
	            }
	        });
		});

		$('.restore-action').on('click', function(e)
		{
			var id = $(this).data('id');
			e.preventDefault();
	        
	        bootbox.confirm({
	            title: 'Confirmation Message',
	            size: "small",
	            message: 'Are you sure you want to restore this Admin',
	            buttons: {
	                cancel: { label: '<i class="fa fa-times"></i> Cancel' },
	                confirm: {
	                    label: '<i class="fa fa-check"></i> Restore',
	                    className: "btn-primary"
	                }
	            },
	            callback: function(result) 
	            {
	                if (result) 
	                {
        				var tbody = $('table#admins tbody');
						var count = tbody.data('count');
						
						$.ajax({
							type: 'POST',
							url: '/admin-panel/admins/all/ajax-restore-admin',
							data: {id: id},
							success: function(response){ 
								$('#admin-row-'+id).fadeOut(); 
								count = count - 1;
								tbody.attr('data-count', count);
							},
							error: function(x) { crud_handle_server_errors(x); },
							complete: function() { 
								// if(count == 1) tbody.append(`<tr><td colspan="5"><strong>No data available in table</strong></td></tr>`);	
							}
						});
	                }
	            }
	        });
		});

		$(function () {
			$('#admins').DataTable({
	      		'paging'      : true,
	      		'lengthChange': true,
	      		'searching'   : true,
			    'ordering'    : true,
			    'info'        : true,
			    'autoWidth'   : true
	    	})
	  	});

	</script>
@stop