@extends('layouts.app')

@section('title', 'My Profile')

@section('style')
<style>
	.bg-navy  { background-color: #374258; color:#fff;  }
	.bg-white { background-color: #fff; color:#374258;  }
</style>
@stop

@section('content')
@if(Session::has('success')) <div class="alert alert-success">{{ Session::get('success') }}</div> @endif
<div class="row wow fadeIn">
	<div class="col-md-9">
		<div class="card bg-navy">
			<div class="card-header"><i class="fa fa-gear"></i> Settings</div>
			<div class="card-body bg-white">
				<form enctype="multipart/form-data" action="{{ route('users.updateprofile', ['id' => $user->id]) }}" method="POST">
					@csrf
					<div class="form-group">
						<label for="first_name">First Name</label>
						<input type="text" name="first_name" class="form-control" value="{{ $user->first_name }}">
					</div>
					<div class="form-group">
						<label for="last_name">Last Name</label>
						<input type="text" name="last_name" class="form-control" value="{{ $user->last_name }}">
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="text" name="email" class="form-control" value="{{ $user->email }}">
					</div>
					<div class="form-group">
						<label for="address">Address</label>
						<input type="text" name="address" class="form-control" value="{{ $user->address }}">
					</div>
					<div class="form-group">
						<label for="phone">Mobile Phone</label>
						<input type="text" name="phone" class="form-control" value="{{ $user->phone }}">
					</div>
					<div class="form-group">
						<label for="department_id">Department</label>
						<select name="department_id" class="form-control">
							<option value="0" selected="selected" disabled="disabled">Select a value</option>
							@foreach (Department()->getInSelectForm() as $key => $value)
								<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="city_id">City</label>
						<select name="city_id" class="form-control">
							<option value="0" selected="selected" disabled="disabled">Select a value</option>
							@foreach (City()->getInSelectForm() as $key => $value)
								<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" name="password" class="form-control">
					</div>
					<div class="form-group">
						<label for="image">Photo</label>
						<input type="file" class="form-control" id="image" accept="image/*" name="image">
					</div>
					<div class="form-group">
						<input type="submit" name="submit" value="Save" class="btn btn-block btn-warning">
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="card bg-navy">
			<div class="card-header">Courses ({{ getUserCourses($auth->id)->count() }})</div>
			<div class="card-body bg-white">
				<ul class="list-group">
				@forelse(getUserCourses($user->id) as $course)
					<li class="list-group-item">{{ $course->name }}</li>
				@empty
				<li class="list-group-item">No Cources</li>
				@endforelse
				</ul>
			</div>
		</div>
		<div class="card bg-navy">
			<div class="card-header">Applyed Jobs ({{ getUserApplyedJobs($auth->id)->count() }})</div>
			<div class="card-body bg-white">
								<ul class="list-group">
				@forelse(getUserApplyedJobs($user->id) as $job)
					<li class="list-group-item">{{ $job->jop->name }}</li>
				@empty
				<li class="list-group-item">No Jobs</li>
				@endforelse
				</ul>
			</div>
		</div>
	</div>
</div>
@stop