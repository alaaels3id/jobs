<div class="form-group">
	@include('includes.backend.inputs', ['errors'=>$errors,'type'=>'text','name'=>'name','style'=>'form-control form-data','col'=>'6'])
	@include('includes.backend.inputs', ['errors'=>$errors,'type'=>'email','name'=>'email','style'=>'form-control form-data','col'=>'6'])
</div>

<div class="form-group">
	@include('includes.backend.inputs', ['errors'=>$errors,'type'=>'text','name'=>'address','style'=>'form-control form-data','col'=>'6'])
	@include('includes.backend.inputs', ['errors'=>$errors,'type'=>'text','name'=>'phone','style'=>'form-control form-data','col'=>'6'])
</div>

@if(Request::is('admin-panel/companies/create'))
	<div class="form-group">
		@include('includes.backend.inputs', ['errors'=>$errors,'type'=>'password','name'=>'password','style'=>'form-control form-data','col'=>'6'])
		@include('includes.backend.inputs', ['errors'=>$errors,'type'=>'password_confirmation','name'=>'password', 'style' => 'form-control form-data'])
	</div>
@endif

<div class="form-group">
	@include('includes.backend.inputs', [
		'errors' => $errors, 'type' => 'select', 
		'name' => 'city_id', 'style' => 'form-control form-data',
		'list' => $list['cities'],'col'=>'6'
	])
	@include('includes.backend.inputs', [
		'errors' => $errors, 'type' => 'select', 
		'name' => 'department_id', 'style' => 'form-control form-data',
		'list' => $list['departments'],'col'=>'6'
	])
</div>

<div class="form-group">
	@include('includes.backend.inputs', ['errors'=>$errors,'type'=>'textarea','name'=>'description','style'=>'form-control form-data','col'=>'6'])
	<div class="col-xs-6 {{ $errors->first('image' , 'has-error') }}">
		<div class="row img-media">
			<div class="col-xs-12">
				<div class="form-valid">
					<label for="image">Image</label>
					<input type="file" class="form-data form-control" id="image" accept="image/*" name="image">
					{!! $errors->first('image', '<div class="help-block text-right animated fadeInDown val-error">:message</div>') !!}
				</div>
			</div>
		</div>
	</div>
</div>