<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateMessageRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'name'    => 'required|string',
            'email'   => 'required|email|exists:users,email',
            'subject' => 'required|string',
            'message' => 'required|string',
        ];

        return $rules;
    }
}
