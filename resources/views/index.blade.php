@extends('layouts.app')

@section('title', 'Dashboard')

@section('style')
<style>
    .bg-navy  { background-color: #374258; color:#fff;  }
    .bg-white { background-color: #fff; color:#374258;  }
</style>
@stop

@section('content')
<main class="mt-5 pt-5">
    @if(Session::has('success'))<div class="alert alert-success">{{ Session::get('success') }}</div>@endif
    @if(Session::has('danger'))<div class="alert alert-danger">{{ Session::get('danger') }}</div>@endif
    <div class="container">
        <section class="mt-4">
            <div class="row">
                <div class="col-md-8 mb-4">
                    <div class="card mb-4 wow fadeIn">
                        <img src="{{ asset('front/img/img(144).jpg') }}" class="img-fluid" alt="">
                    </div>
                    <div class="card mb-4 wow fadeIn">
                        <div class="card-body text-center">
                            <p class="h5 my-4">What is CTJ?</p>
                            <p>
                                CTJ is the first employment site in the field of computer science, providing job opportunities for students and saving time and effort for both student and the company to work together.
                            </p>
                            <h5 class="my-4">
                                <strong>CTJ - trusted by 400 000 + developers &amp; designers</strong>
                            </h5>
                            <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" data-interval="1800">
                                <div class="carousel-inner">
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                                <img src="{{ asset('front/img/markers/nike.png') }}" 
                                                    class="img-fluid px-4" alt="Nike - logo" style="max-height: 40px">
                                            </div>
                                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                                <img src="{{ asset('front/img/markers/amazon.png') }}" class="img-fluid px-4" alt="Amazon - logo" style="max-height: 40px">
                                            </div>
                                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                                <img src="{{ asset('front/img/markers/sony.png') }}" class="img-fluid px-4" alt="Sony - logo"  style="max-height: 40px">
                                            </div>
                                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                                <img src="{{ asset('front/img/markers/samsung.png') }}" class="img-fluid px-4" alt="Samsung - logo" style="max-height: 40px">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="carousel-item active carousel-item-left">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                                <img src="{{ asset('front/img/markers/airbus.png') }}" class="img-fluid px-4" alt="Airbus - logo" style="max-height: 40px">
                                            </div>
                                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                                <img src="{{ asset('front/img/markers/yahoo.png') }}" class="img-fluid px-4" alt="Yahoo - logo" style="max-height: 40px">
                                            </div>
                                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                                <img src="{{ asset('front/img/markers/deloitte.png') }}" class="img-fluid px-4" alt="Deloitte - logo" style="max-height: 40px">
                                            </div>
                                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                                <img src="{{ asset('front/img/markers/ge.png') }}" class="img-fluid px-4" alt="GE - logo" style="max-height: 40px">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="carousel-item carousel-item-next carousel-item-left">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                                <img src="{{ asset('front/img/markers/kpmg.png') }}" class="img-fluid px-4" alt="KPMG - logo" style="max-height: 40px">
                                            </div>
                                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                                <img src="{{ asset('front/img/markers/unity.png') }}" class="img-fluid px-4" alt="Unity - logo" style="max-height: 40px">
                                            </div>
                                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                                <img src="{{ asset('front/img/markers/ikea.png') }}" class="img-fluid px-4" alt="Ikea - logo" style="max-height: 40px">
                                            </div>
                                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                                <img src="{{ asset('front/img/markers/aegon.png') }}" class="img-fluid px-4" alt="Aegon - logo" style="max-height: 40px">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>

                    @forelse(getCompanyJops() as $com)
                    {!! Form::open(['url'=>url('/user/'.$auth->id.'/jop/'.$com->jop_id), 'method'=>'POST']) !!}
                    <div class="card mb-4 wow fadeIn">
                        <div class="card-header bg-navy">{{ $com->company->name }} posted a new job</div>
                        <div class="card-body bg-white">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-center" style="float: left;">
                                        <img src="{{ getImage('company', $com->company->image) }}" class="rounded-circle" width="90" height="90" alt="">
                                    </p>
                                    <p style="float: right;">{{ $com->company->name }}</p>
                                </div>
                                <div class="col-md-12">
                                    <blockquote class="blockquote">
                                        <p class="mb-0 lead">
                                            {{ $com->company->name }} is wanted a {{ $com->jop->name }} <br>
                                            with a salary {{ $com->jop->salary }} and {{ $com->jop->description }}
                                        </p>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            @if($com->company->department_id == $auth->department_id)
                                @if (!isUserApplyed($auth->id, $com->jop_id))
                                    <input type="submit" name="submit" class="btn btn-danger btn-md" value="Apply Now!">
                                @else
                                    <button disabled class="btn btn-success btn-md">Applyed</button>  
                                @endif
                            @endif
                        </div>
                    </div>
                    {!! Form::close() !!}
                    @empty
                    No Companies Jobs
                    @endforelse

                    <div class="card card-img mb-4 wow fadeIn" style="background-image:url('{{ asset('front/img/blog1.jpg') }}');">
                        <div class="card-body">
                            <div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">
                                <div>
                                    <h5 class="deep-orange-text"><i class="fa fa-pie-chart"></i> Marketing</h5>
                                    <h3 class="card-title pt-2"><strong>This is card title</strong></h3>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat, laboriosam, voluptatem,
                                        optio vero odio nam sit officia accusamus minus error nisi architecto nulla ipsum dignissimos.
                                        Odit sed qui, dolorum!.
                                    </p>
                                    <a class="btn btn-deep-orange"><i class="fa fa-clone left"></i>Read More....</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 mb-4">
                    <div class="card morpheus-den-gradient mb-4 wow fadeIn">
                        <div class="card-body text-white text-center">
                            <h4 class="mb-4"><strong>Find Your Job with CTJ</strong></h4>
                            <p><strong>Best & free guide of responsive employment</strong></p>
                            <p class="mb-4">
                                <strong>
                                    The most comprehensive tutorial for the Job. Loved by over 500 000 users. Video
                                    and written versions available. Create your own, stunning Portfolio.
                                </strong>
                            </p>
                            <a target="_blank" href="{{ route('users.jops') }}" class="btn btn-outline-white btn-md">Join our community
                                <i class="fa fa-graduation-cap ml-2"></i>
                            </a>
                        </div>
                    </div>
                    
                    @guest()
                    <div class="card mb-4 text-center wow fadeIn">
                        <div class="card-header">Do you want to get informed about new articles?</div>
                        <div class="card-body">
                            <form>
                                <label for="defaultFormEmailEx" class="grey-text">Your email</label>
                                <input type="email" id="defaultFormLoginEmailEx" class="form-control">
                                <br>
                                <label for="defaultFormNameEx" class="grey-text">Your name</label>
                                <input type="text" id="defaultFormNameEx" class="form-control">
                                <div class="text-center mt-4">
                                    <button class="btn btn-danger btn-md" type="submit">Sign up</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    @else
                    <div class="card mb-4 text-center wow fadeIn">
                        <div class="card-header text-danger"> Your Profile Info </div>
                        <div class="card-body">
                            <p><b>Name :</b>{{ ucwords($auth->fullname) }}</p>
                            <p><b>E-mail:</b>{{ $auth->email }}</p>
                            <p><b>Since:</b> {{ $auth->created_at->format('d-m-Y') }}</p>
                            <p><b>Department:</b> {{ $auth->department->name }}</p>
                            <div class="text-center mt-4">
                                <a href="{{ route('users.profile',['id'=>$auth->id]) }}" class="btn btn-danger btn-md">Update</a>
                            </div>
                        </div>
                    </div>
                    @endguest
                </div>
            </div>
        </section>
    </div>
</main>
@stop