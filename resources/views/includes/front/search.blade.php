@extends('layouts.app')

@section('title', 'Available Jobs')

@section('style')
<style>
    .bg-navy  { background-color: #374258; color:#fff;  }
    .bg-white { background-color: #fff; color:#374258;  }
    .company-image { border-radius: 50%; padding: 5px; margin: 5px; display: inline-block;}
</style>
@stop

@section('content')
<div class="row wow fadeIn">
    <div class="col-md-9">
        @forelse($results as $key => $result)

        <br>
        <div class="card bg-navy">
            <div class="card-header">Best Match</div>
            <form action="#" method="POST">
                @csrf
                <div class="card-body bg-white">
                    @php
                        $jop = jop()->find($result->id);
                    @endphp
                    <b>Job Name</b> : {{ ucwords($jop->name) }} <br>
                    <b>Since</b> : {{ $result->created_at->diffForHumans() }} <br>
                    <b>Job Description</b> : {!! getDeafultValue(ucwords($jop->description)) !!} <br>
                    <b>Job Salary</b> : {{ $jop->salary }} L.E <br>
                </div>
                <div class="card-footer">
                    <div class="form-group">
                        @if (!isUserApplyed($auth->id, $jop->id))
                            <input type="submit" name="submit" class="btn btn-danger btn-md" value="Apply Now!">
                        @else
                            <button disabled class="btn btn-success btn-md">Applyed</button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
        <br>
        @empty
        <div class="card bg-navy">
            <div class="card-header">Best Match</div>
            <div class="card-body bg-white">No JOBS</div>
        </div>
        @endforelse
    </div>
    <div class="col-md-3">
        @guest()
        <div class="card mb-4 text-center wow fadeIn">
            <div class="card-header">Do you want to get informed about new articles?</div>
            <div class="card-body">
                <form>
                    <label for="defaultFormEmailEx" class="grey-text">Your email</label>
                    <input type="email" id="defaultFormLoginEmailEx" class="form-control">
                    <br>
                    <label for="defaultFormNameEx" class="grey-text">Your name</label>
                    <input type="text" id="defaultFormNameEx" class="form-control">
                    <div class="text-center mt-4">
                        <button class="btn btn-danger btn-md" type="submit">Sign up</button>
                    </div>
                </form>
            </div>
        </div>
        @else
        <div class="card mb-4 text-center wow fadeIn">
            <div class="card-header text-danger"> Your Profile Info </div>
            <div class="card-body">
                <p><b>Name :</b>{{ ucwords($auth->fullname) }}</p>
                <p><b>E-mail:</b>{{ $auth->email }}</p>
                <p><b>Since:</b> {{ $auth->created_at->format('d-m-Y') }}</p>
                <p><b>Department:</b> {{ $auth->department->name }}</p>
                <div class="text-center mt-4">
                    <a href="{{ route('users.profile',['id'=>$auth->id]) }}" class="btn btn-danger btn-md">Update</a>
                </div>
            </div>
        </div>
        @endguest
    </div>
</div>
@stop