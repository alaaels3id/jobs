<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCompanyRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'city_id'       => 'required|numeric|exists:cities,id',
            'department_id' => 'required|numeric|exists:departments,id',
            'name'          => 'required|string',
            'address'       => 'required|string',
            'phone'         => 'required|numeric',
            'description'   => 'required|string',
            'image'         => 'nullable|image|mimes:jpg,png,jpeg',
            'email'         => 'required|email|unique:companies,email',
            'password'      => 'required|confirmed',
        ];

        return $rules;
    }
}
