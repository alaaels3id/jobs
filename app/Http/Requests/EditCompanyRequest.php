<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Company;

class EditCompanyRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $company  = Company::find($this->company);
        $rules = [
            'city_id'       => 'required|numeric|exists:cities,id',
            'department_id' => 'required|numeric|exists:departments,id',
            'name'          => 'required|string',
            'address'       => 'required|string',
            'description'   => 'required|string',
            'phone'         => 'required|numeric',
            'image'         => 'nullable|image|mimes:jpg,png,jpeg',
            'email'         => 'required|email|unique:companies,email,'.$company->id,
        ];

        return $rules;
    }
}
