<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateSettingRequest;
use App\Http\Requests\EditSettingRequest;
use App\Models\Setting;
use Auth, DB, Image, File, Validator, Response, Hash;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::all();
        return view('Backend.settings.index', compact('settings'));
    }

    public function home()
    {
        return view('Backend.settings.home');
    }

    public function Trashed()
    {
        $settings = Setting::onlyTrashed()->get();
        return view('Backend.settings.trashed', compact('settings'));
    }

    public function create()
    {
        return view('Backend.settings.create');
    }

    public function store(CreateSettingRequest $request)
    {
        $settingData = $request->all();

        $settingData['admin_id'] = auth()->user()->id;

        if(Setting::createSetting($settingData))
        {
            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Added Successfully')]);
            } else {
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Added Successfully'));
            }
        }
        else
        {
            if(request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Internal Server Error 500')]);
            } else {
                request()->session()->flash('status','danger');
                request()->session()->flash('message',__('Internal Server Error 500'));
            }

            return back()->withInputs();
        }
        return redirect()->route('settings.index');
    }

    public function ChangeSettingStatus(Request $request)
    {
        // Request ajax from index;
        $settingData = $request->all();
        $currentSetting = Setting::find($request->id);
        
        if (!$updatesettingstatus = Setting::updateSettingStatus($settingData, $currentSetting))
            return response()->json(['requestStatus' => false, 'message' => trans('Server Internal Error 500')]);

        return response()->json(['requestStatus' => true, 'message' => trans('Data Updated Successfully')]);
    }

    public function edit($id)
    {
        $setting = Setting::find($id);
        return view('Backend.settings.edit', compact('setting'));
    }

    public function update(EditSettingRequest $request, $id)
    {
        $settingData = $request->all();
        $settingData['admin_id'] = auth()->user()->id;
        $currentSetting = Setting::find($id);
        
        if (Setting::updateSetting($settingData, $currentSetting)) {

            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Updated Successfully')]);
            }else{
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Updated Successfully'));
            }
        } else {
            if (request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Server Internal Error 500')]);
            } else {
                 request()->session()->flash('status','danger');
                 request()->session()->flash('message',__('Server Internal Error 500'));
            }
        }

        return redirect()->route('settings.edit', ['id' => $currentSetting->id]);
    }

    public function DeleteSetting(Request $request)
    {
        $setting = Setting::find($request->id);

        if(!$setting) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, Setting is not exists !!']);

        try
        {            
            $setting->delete();
            
            return response()->json(['deleteStatus' => true, 'message' => 'Setting Deleted Successfully']);
        }
        catch (Exception $e){ return response()->json(['deleteStatus' => false,'error' => 'Server Internal Error 500']); }

        return redirect()->route('settings.index');
    }

    public function RestoreSetting(Request $request)
    {
        $restore = DB::table('settings')->where('id', $request->id)->where('deleted_at', '!=', null)->update(['deleted_at' => null]);
        
        $response['requestStatus']  = ($restore) ? true : false;
        $response['message']        = ($restore) ? 'Setting Restored Successfully' : 'Server Internal Error 500';

        return response()->json($response);
    }

    public function RemoveSetting(Request $request)
    {
        $remove = DB::table('settings')->where('id', $request->id)->where('deleted_at', '!=', null);

        if (isset($remove->image) && $remove->image != null) File::delete('uploaded/settings/'.$remove->image);

        $remove->delete();
        
        $response['requestStatus']  = ($remove) ? true : false;

        return response()->json($response);
    }
}
