@extends('backend.layouts.app')

@section('title', 'Deleted Companies')

@section('styles')
	<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@stop

@section('content')
    
    <section class="content-header">
        <h1>Dashboard<small>Control panel</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Trashed</li>
        </ol>
    </section>

    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box">
    				<div class="box-header">
    					<h3 class="box-title">Data Table With Full Features</h3>
    				</div>
    				<!-- /.box-header -->
    				<div class="box-body">
    					<table id="companies" class="table table-bordered table-striped text-center">
    						
    						<thead>
    							<tr>
    								<th>#</th>
				                  	<th>Name</th>
				                  	<th>Email</th>
				                  	<th>Actions</th>
				                </tr>
	                		</thead>
	                		
	                		<tbody data-count={{ $companies->count() }}>
			                	@foreach($companies as $key => $company)
				                <tr id="company-row-{{ $company->id }}">
				                	<td>{{ (int)$key+1 }}</td>
				                	<td>{{ ucwords($company->name) }}</td>
				                	<td><a href="mailto:{{ $company->email }}">{{ $company->email }}</a></td>
				                	
				                	<td>
				                		<ul class="list-unstyled list-inline">
				                			<li>
				                				<a 
				                					href="{{ url('/companies/'.$company->id) }}" 

				                					data-toggle='tooltip' title="Restore"

				                					data-id="{{ $company->id }}" 

				                					class="btn btn-primary btn-sm restore-action">
				                					
				                					<i class="fa fa-repeat"></i>
				                				</a>
				                			</li>

				                			<li>
				                				<a 
				                					href='{{ url('/companies/'.$company->id) }}' 
				                					
				                					class='btn btn-sm btn-danger delete-action' 
				                					
				                					type='button' data-id="{{ $company->id }}"

				                					data-toggle='tooltip' title='Delete'>

				                					<i class='fa fa-times'></i>
												</a>
				                			</li>
				                		</ul>
				                	</td>
				                </tr>
			                	@endforeach
	                		</tbody>
	                		
	                		<tfoot>
	                			<tr>
    								<th>#</th>
				                  	<th>Name</th>
				                  	<th>Email</th>
				                  	<th>Actions</th>
	                			</tr>
	                		</tfoot>
	              		
	              		</table>
	            	</div>
	            	<!-- /.box-body -->
	          	</div>
            </div>
          </div>
        </div>
      </div>
    </section>

@stop

@section('scripts')
	<script src="{{ asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script>

		$('.delete-action').on('click', function(e)
		{
			var id = $(this).data('id');
			e.preventDefault();
	        
	        bootbox.confirm({
	            title: 'Confirmation Message',
	            size: "small",
	            message: 'Are you sure you want to remove this company',
	            buttons: {
	                cancel: {
	                    label: '<i class="fa fa-times"></i> Cancel'
	                },
	                confirm: {
	                    label: '<i class="fa fa-check"></i> Confirm',
	                    className: "btn-danger"
	                }
	            },
	            callback: function(result) 
	            {
	                if (result) 
	                {
						$.ajax({
							type: 'POST',
							url: '/admin-panel/companies/all/ajax-remove-company',
							data: {id: id},
							success: function(response){ $('#company-row-'+id).fadeOut(); },
							error: function(x) { crud_handle_server_errors(x); },
							complete: function() {
								var tbody = $('table#companies tbody');
								var count = tbody.data('count');
								if(count == 1) tbody.append(`<tr><td colspan="5"><strong>No Trashed</strong></td></tr>`);
							}
						});
	                }
	            }
	        });
		});

		$('.restore-action').on('click', function(e)
		{
			var id = $(this).data('id');
			e.preventDefault();
	        
	        bootbox.confirm({
	            title: 'Confirmation Message',
	            size: "small",
	            message: 'Are you sure you want to restore this company',
	            buttons: {
	                cancel: {
	                    label: '<i class="fa fa-times"></i> Cancel'
	                },
	                confirm: {
	                    label: '<i class="fa fa-check"></i> Restore',
	                    className: "btn-primary"
	                }
	            },
	            callback: function(result) 
	            {
	                if (result) 
	                {
						$.ajax({
							type: 'POST',
							url: '/admin-panel/companies/all/ajax-restore-company',
							data: {id: id},
							success: function(response){ $('#company-row-'+id).fadeOut(); },
							error: function(x) { crud_handle_server_errors(x); },
							complete: function() { 
								var tbody = $('table#companies tbody');
								var count = tbody.data('count');
								if(count == 1) tbody.append(`<tr><td colspan="5"><strong>No Trashed</strong></td></tr>`);	
							}
						});
	                }
	            }
	        });
		});

		$(function () {
			$('#companies').DataTable({
	      		'paging'      : true,
	      		'lengthChange': true,
	      		'searching'   : true,
			    'ordering'    : true,
			    'info'        : true,
			    'autoWidth'   : true
	    	})
	  	});

	</script>
@stop