<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdminRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'city_id'    => 'required|numeric|exists:cities,id',
            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'address'    => 'required|string',
            'phone'      => 'required|numeric',
            'image'      => 'nullable|image|mimes:jpg,png,jpeg',
            'email'      => 'required|email|unique:users,email',
            'password'   => 'required|confirmed',
        ];

        return $rules;
    }
}
