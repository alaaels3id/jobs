@extends('layouts.app')

@section('title', 'Jobs')

@section('style')
<style>
	.bg-navy  { background-color: #374258; color:#fff;  }
	.bg-white { background-color: #fff; color:#374258;  }
	.company-image { border-radius: 50%; padding: 5px; margin: 5px; display: inline-block;}
</style>
@stop

@section('content')
<div class="row wow fadeIn">
	<div class="col-md-8">
		@forelse($jobs as $jop)
		<br>
		<div class="card bg-navy">
			<div class="card-header">Our Company's Jobs</div>
			<form action="#" method="POST">
				@csrf
				<div class="card-body bg-white">
					<img src="{{ getImage('company',$jop->company->image) }}" class="company-image" width="60" height="60" alt="">
					<b>Job Name</b> : {{ ucwords($jop->jop->name) }} <br>
					<b>Company Name</b> : {{ ucwords($jop->company->name) }} <br>
					<b>Since</b> : {{ $jop->created_at->diffForHumans() }} <br>
					<b>Company Description</b> : {!! getDeafultValue($jop->company->description) !!} <br>
					<b>Company Address</b> : {!! getDeafultValue($jop->company->address) !!} <br>
					<b>Company Email</b> : {{ $jop->company->email }} <br>
					<b>Company Location</b> : {{ $jop->company->city->name }} <br>
					<b>Company Department</b> : {{ $jop->company->department->name }} <br>
					<b>Company Phone</b> : {!! getDeafultValue($jop->company->phone) !!} <br>
					<b>Job Description</b> : {!! getDeafultValue(ucwords($jop->jop->description)) !!} <br>
					<b>Job Salary</b> : {{ $jop->jop->salary }} L.E <br>
				</div>
				<div class="card-footer"></div>
			</form>
		</div>
		<br>
		@empty
		<div class="card bg-navy">
			<div class="card-header">Our Company's Jobs</div>
			<div class="card-body bg-white">No JOBS</div>
		</div>
		@endforelse
	</div>
	<div class="col-md-4">
        <div class="card mb-4 text-center wow fadeIn">
            <div class="card-header text-danger"> Your Profile Info </div>
            <div class="card-body">
                <p><b>Name :</b>{{ ucwords($auth->name) }}</p>
                <p><b>E-mail:</b>{{ $auth->email }}</p>
                <p><b>Since:</b> {{ $auth->created_at->format('d-m-Y') }}</p>
                <p><b>Department:</b> {{ $auth->department->name }}</p>
                <div class="text-center mt-4">
                    <a href="{{ route('users.profile',['id'=>$auth->id]) }}" class="btn btn-danger btn-md">Update</a>
                </div>
            </div>
        </div>
	</div>
</div>
@stop