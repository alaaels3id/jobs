<?php

/**
 *
	 	.bg-red, .bg-yellow, .bg-aqua, .bg-blue, .bg-light-blue, .bg-green, .bg-navy, .bg-teal, .bg-olive, .bg-lime, .bg-orange, .bg-fuchsia, .bg-purple, .bg-maroon, .bg-black, .bg-red-active, .bg-yellow-active, .bg-aqua-active, .bg-blue-active, .bg-light-blue-active, .bg-green-active, .bg-navy-active, .bg-teal-active, .bg-olive-active, .bg-lime-active, .bg-orange-active, .bg-fuchsia-active, .bg-purple-active, .bg-maroon-active
 *
 * 
 */

function getImage($type, $img)
{
	if($type == 'user') return ($img != null) ? asset('uploaded/users/'.$img) : asset('admin/img/avatar10.jpg');
	
	elseif($type == 'admin') return ($img != null) ? asset('uploaded/admins/'.$img) : asset('admin/img/avatar10.jpg');
	 
	elseif($type == 'company') return ($img != null) ? asset('uploaded/companies/'.$img) : asset('admin/img/avatar10.jpg');
	
	else return asset('admin/img/avatar10.jpg');
}

function removeTagsFromText($text)
{
	if($text == null) return trans('Backend.none');
	$pure 		= strip_tags($text);
	$filtered 	= preg_replace("/\s|&nbsp;/",' ',$pure);
	$final 		= preg_replace("/\s|&#39;/",' ',$filtered);
	return ucwords($final);
}

function getModelCount($model, $withDeleted = false)
{
	if($withDeleted){
		if($model == 'admin') return App\Models\Admin::onlyTrashed()->where('is_super_Admin', '!=', 1)->count();
		$mo = "App\\Models\\$model";
		return $mo::onlyTrashed()->count();
	}
	if($model == 'admin') return App\Models\Admin::where('is_super_Admin', '!=', 1)->count();
	$mo = "App\\Models\\$model";
	return $mo::count();
}

function plural($text, $uppercase = true) { return $uppercase == false ? str()->plural($text) : ucwords(str()->plural($text)); }

function limit($text, $limit){ return ucwords(str_limit($text, $limit, ' ...')); }

function str() { return new Illuminate\Support\Str; }
function carbon() { return new \Carbon\Carbon; }
function user() { return new App\Models\User; }
function admin() { return new App\Models\Admin; }
function jop() { return new App\Models\Jop; }
function course() { return new App\Models\Course; }
function company() { return new App\Models\Company; }
function company_jop() { return new App\Models\CompanyJop; }
function jop_user() { return new App\Models\JopUser; }
function department() { return new App\Models\Department; }
function city() { return new App\Models\City; }
function contact() { return new App\Models\Contact; }

function models()
{
    $models[0] = ['user',  'ion-ios-contact-outline'     ,'bg-red'];
    $models[1] = ['admin', 'ion-android-person','bg-yellow'];
    $models[2] = ['company', 'ion-arrow-graph-up-right','bg-aqua'];
    $models[3] = ['jop', 'ion-briefcase','bg-light-blue'];
    $models[4] = ['department', 'ion-ios-filing-outline','bg-green'];
    $models[5] = ['course', 'ion-ios-copy','bg-orange'];
    $models[6] = ['city', 'ion-ios-flag','bg-teal'];
    $models[7] = ['setting', 'ion-gear-b','bg-maroon'];
    $models[8] = ['contact', 'ion-gear-b','bg-fuchsia'];

    return $models;
}

function getModelWithIcon($model)
{
    $models['user']  = ['ion-ios-contact-outline'	  ,'bg-red'];
    $models['admin'] = ['ion-android-person','bg-yellow'];
    $models['company'] = ['ion-arrow-graph-up-right','bg-aqua'];
    $models['jop'] = ['ion-briefcase','bg-light-blue'];
    $models['department'] = ['ion-ios-filing-outline','bg-green'];
    $models['course'] = ['ion-ios-copy','bg-orange'];
    $models['city'] = ['ion-ios-flag','bg-teal'];
    $models['setting'] = ['ion-gear-b','bg-maroon'];
    $models['contact'] = ['ion-gear-b','bg-fuchsia'];
    
    return $models[$model];
}

function getUserCourses($user_id)
{
	$user = user()->find($user_id);
	$courses = course()->where('user_id', $user_id)->get();
	return $courses;
}

function getUserCompanies()
{
	return company()->status(1)->take(8)->latest()->get();
}

function getCompanyJops($company_id = '')
{
	return company_jop()->when($company_id,function($query, $company_id){
		return ($company_id == '') ? $query : $query->where('company_id', $company_id);
	})->get();
}

function getAvailableCompaniesJobs($user_id)
{
	$user = user()->find($user_id);

	$companiesIds = company()->where('department_id', $user->department_id)->get()->pluck('id')->toArray();

	return company_jop()->whereIn('company_id', $companiesIds)->get();
}

function getDeafultValue($value)
{
	return ($value == '' || $value == null) 
		? '<code>NO DATA</code>' 
		: ucwords($value);
}

function isUserApplyed($user_id, $jop_id)
{
	$check = jop_user()->where('user_id', $user_id)->where('jop_id', $jop_id)->count();
	return $check > 0 ? true : false;
}

function getUserApplyedJobs($user_id)
{
	$jops = jop_user()->where('user_id', $user_id)->get();
	return $jops;
}