@if($type == 'password_confirmation')

	<div class="col-xs-{{ $col }} {{ $errors->first($name , 'has-error') }}">
		<div class="form-valid floating">
			<label for="password_confirmation">Confirm</label>
			{!! Form::password('password_confirmation', ['class'=>'form-control form-data','id' => 'password_confirmation']) !!}
		</div>
		{!! $errors->first('password' , '<div class="help-block text-right animated fadeInDown val-error">:message</div>') !!}
	</div>

@elseif($type == 'password')

	<div class="col-xs-{{ $col }} {{ $errors->first($name , 'has-error') }}">
		<div class="form-valid floating">
			<label for="{{ $name }}">{{ ucwords($name) }}</label>
			{!! Form::password($name, ['class'=>'form-control form-data','id' => $name]) !!}
		</div>
		{!! $errors->first('password' , '<div class="help-block text-right animated fadeInDown val-error">:message</div>') !!}
	</div>

@elseif($type == 'select')

	<div class="col-xs-{{ $col }} {{ $errors->first($name , 'has-error') }}">
		<div class="form-valid floating">
			<label for="{{ $name }}">{{ ucwords(explode('_', $name)[0]) }}</label>
			{!! Form::select($name, $list, 'Select a value', ['class'=>'form-control form-data','id' => $name]) !!}
		</div>
		{!! $errors->first($name , '<div class="help-block text-right animated fadeInDown val-error">:message</div>') !!}
	</div>

@else

	<div class="col-xs-{{ $col }} {{ $errors->first($name , 'has-error') }}">
		<div class="form-valid floating">
			<label for="{{ $name }}">{{ ucwords(str_replace('_', ' ',$name)) }}</label>
			{!! Form::{$type}($name, null, ['class'=>$style,'id' => $name, 'autocomplete' => 'off']) !!}
			{!! $errors->first($name , '<div class="help-block text-right animated fadeInDown val-error">:message</div>') !!}
		</div>
	</div>

@endif
