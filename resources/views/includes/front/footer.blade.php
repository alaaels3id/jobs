<!-- Footer -->
<footer class="page-footer text-center font-small  pt-4">
    <!-- Footer Elements -->
    <div class="container">
        <!-- Social buttons -->
        <div class="pb-4">
            <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook mr-3"></i></a>
            <a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter mr-3"></i></a>
            <a href="https://www.youtube.com/watch?v=7MUISDJ5ZZ4" target="_blank"><i class="fa fa-youtube mr-3"></i></a>
            <a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus mr-3"></i></a>
            <a href="https://dribbble.com/" target="_blank"><i class="fa fa-dribbble mr-3"></i></a>
            <a href="https://pinterest.com/" target="_blank"><i class="fa fa-pinterest mr-3"></i></a>
            <a href="https://github.com/" target="_blank"><i class="fa fa-github mr-3"></i></a>
            <a href="http://codepen.io/" target="_blank"><i class="fa fa-codepen mr-3"></i></a>
        </div>
        <!-- Social buttons -->
    </div>
    <!-- Footer Elements -->
    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
        <a href="contact us.html"> C.T.Jobs.com </a>
    </div>
    <!-- Copyright -->
</footer>