<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateMessageRequest;
use App\Models\Contact;

class ContactController extends Controller
{
    public function index()
    {
    	$contacts = Contact::all();
    	return view('backend.contacts.index', compact('contacts'));
    }

    public function store(CreateMessageRequest $request)
    {
    	$messageData = $request->all();
    	
        if(Contact::createMessage($messageData))
        {
            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Added Successfully')]);
            } else {
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Added Successfully'));
            }
        }
        else
        {
            if(request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Internal Server Error 500')]);
            } else {
                request()->session()->flash('status','danger');
                request()->session()->flash('message',__('Internal Server Error 500'));
            }

            return back()->withInputs();
        }
        return redirect()->back();
    }
}
