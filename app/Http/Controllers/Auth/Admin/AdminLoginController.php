<?php

namespace App\Http\Controllers\Auth\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;
    
	public function __construct()
	{
		$this->middleware('guest:admin')->except('adminLogout');
	}

    public function showAdminLoginForm()
    {
        return view('auth.pages.adminLogin');
    }

    public function adminLogin(Request $request)
    {
        $validation['email']    = 'required|email';
        $validation['password'] = 'required|string|min:6';

        $credentials['email']    = $request->email;
        $credentials['password'] = $request->password;
        $credentials['status']   = 1;
        
        $this->validate($request, $validation);

        if (Auth::guard('admin')->attempt($credentials)) {

            return redirect()->intended('/admin-panel');
        }
        return back()->withInput($request->only('email'));
    }

    public function adminLogout()
    {
        Auth::guard('admin')->logout();

        // in case of multi auth, comment this two lines
        // request()->session()->flush();
        // request()->session()->regenerate();
 
        return redirect('/admin-panel');
    }
}
