<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateJopRequest;
use App\Http\Requests\EditJopRequest;
use App\Models\Jop;
use Auth, DB, Image, File, Validator, Response, Hash;

class JopController extends Controller
{
    public function index()
    {
        $jops = Jop::all();
        return view('Backend.jops.index', compact('jops'));
    }

    public function home()
    {
        return view('Backend.jops.home');
    }

    public function Trashed()
    {
        $jops = Jop::onlyTrashed()->get();
        return view('Backend.jops.trashed', compact('jops'));
    }

    public function create()
    {
        return view('Backend.jops.create');
    }

    public function store(CreateJopRequest $request)
    {
        $jopData = $request->all();

        if(Jop::createJop($jopData))
        {
            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Added Successfully')]);
            } else {
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Added Successfully'));
            }
        }
        else
        {
            if(request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Internal Server Error 500')]);
            } else {
                request()->session()->flash('status','danger');
                request()->session()->flash('message',__('Internal Server Error 500'));
            }

            return back()->withInputs();
        }
        return redirect()->route('jops.index');
    }

    public function Changejopstatus(Request $request)
    {
        // Request ajax from index;
        $jopData = $request->all();
        $currentJop = Jop::find($request->id);
        
        if (!$updatejopstatus = Jop::updatejopstatus($jopData, $currentJop))
            return response()->json(['requestStatus' => false, 'message' => trans('Server Internal Error 500')]);

        return response()->json(['requestStatus' => true, 'message' => trans('Data Updated Successfully')]);
    }

    public function edit($id)
    {
        $jop = Jop::find($id);
        return view('Backend.jops.edit', compact('jop'));
    }

    public function update(EditJopRequest $request, $id)
    {
        $jopData = $request->all();
        $currentJop = Jop::find($id);
        
        if (Jop::updateJop($jopData, $currentJop) ) {

            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Updated Successfully')]);
            }else{
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Updated Successfully'));
            }
        }else{
            if (request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Server Internal Error 500')]);
            }else{
                 request()->session()->flash('status','danger');
                 request()->session()->flash('message',__('Server Internal Error 500'));
            }
        }

        return redirect()->route('jops.edit', ['id' => $currentJop->id]);
    }

    public function DeleteJop(Request $request)
    {
        $jop = Jop::find($request->id);

        if(!$jop) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, Jop is not exists !!']);

        try
        {            
            $jop->delete();
            
            return response()->json(['deleteStatus' => true, 'message' => 'Jop Deleted Successfully']);
        }
        catch (Exception $e){ return response()->json(['deleteStatus' => false,'error' => 'Server Internal Error 500']); }

        return redirect()->route('jops.index');
    }

    public function RestoreJop(Request $request)
    {
        $restore = DB::table('jops')->where('id', $request->id)->where('deleted_at', '!=', null)->update(['deleted_at' => null]);
        
        $response['requestStatus']  = ($restore) ? true : false;
        $response['message']        = ($restore) ? 'Jop Restored Successfully' : 'Server Internal Error 500';

        return response()->json($response);
    }

    public function RemoveJop(Request $request)
    {
        $remove = DB::table('jops')->where('id', $request->id)->where('deleted_at', '!=', null);

        if (isset($remove->image) && $remove->image != null) File::delete('uploaded/jops/'.$remove->image);

        $remove->delete();
        
        $response['requestStatus']  = ($remove) ? true : false;

        return response()->json($response);
    }
}
