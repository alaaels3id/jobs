<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCityRequest;
use App\Http\Requests\EditCityRequest;
use App\Models\City;
use Auth, DB, Image, File, Validator, Response, Hash;

class CityController extends Controller
{
    public function index()
    {
        $cities = City::all();
        return view('Backend.cities.index', compact('cities'));
    }

    public function home()
    {
        return view('Backend.cities.home');
    }

    public function Trashed()
    {
        $cities = City::onlyTrashed()->get();
        return view('Backend.cities.trashed', compact('cities'));
    }

    public function create()
    {
        return view('Backend.cities.create');
    }

    public function store(CreateCityRequest $request)
    {
        $cityData = $request->all();

        if(City::createCity($cityData))
        {
            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Added Successfully')]);
            } else {
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Added Successfully'));
            }
        }
        else
        {
            if(request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Internal Server Error 500')]);
            } else {
                request()->session()->flash('status','danger');
                request()->session()->flash('message',__('Internal Server Error 500'));
            }

            return back()->withInputs();
        }
        return redirect()->route('cities.index');
    }

    public function ChangeCityStatus(Request $request)
    {
        // Request ajax from index;
        $cityData = $request->all();
        $currentCity = City::find($request->id);
        
        if (!$updateCityStatus = City::updateCityStatus($cityData, $currentCity))
            return response()->json(['requestStatus' => false, 'message' => trans('Server Internal Error 500')]);

        return response()->json(['requestStatus' => true, 'message' => trans('Data Updated Successfully')]);
    }

    public function edit($id)
    {
        $city = City::find($id);
        return view('Backend.cities.edit', compact('city'));
    }

    public function update(EditCityRequest $request, $id)
    {
        $cityData = $request->all();
        $currentCity = City::find($id);
        
        if (City::updateCity($cityData, $currentCity) ) {

            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Updated Successfully')]);
            }else{
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Updated Successfully'));
            }
        }else{
            if (request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Server Internal Error 500')]);
            }else{
                 request()->session()->flash('status','danger');
                 request()->session()->flash('message',__('Server Internal Error 500'));
            }
        }

        return redirect()->route('cities.edit', ['id' => $currentCity->id]);
    }

    public function DeleteCity(Request $request)
    {
        $city = City::find($request->id);

        if(!$city) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, City is not exists !!']);

        try
        {
            if ($city->image) File::delete('uploaded/cities/'.$city->image);
            
            $city->delete();
            
            return response()->json(['deleteStatus' => true, 'message' => 'City Deleted Successfully']);
        }
        catch (Exception $e){ return response()->json(['deleteStatus' => false,'error' => 'Server Internal Error 500']); }

        return redirect()->route('cities.index');
    }

    public function RestoreCity(Request $request)
    {
        $restore = DB::table('cities')->where('id', $request->id)->where('deleted_at', '!=', null)->update(['deleted_at' => null]);
        
        $response['requestStatus']  = ($restore) ? true : false;
        $response['message']        = ($restore) ? 'City Restored Successfully' : 'Server Internal Error 500';

        return response()->json($response);
    }

    public function RemoveCity(Request $request)
    {
        $remove = DB::table('cities')->where('id', $request->id)->where('deleted_at', '!=', null);

        if (isset($remove->image) && $remove->image != null) File::delete('/uploaded/cities/'.$remove->image);

        $remove->delete();
        
        $response['requestStatus']  = ($remove) ? true : false;

        return response()->json($response);
    }
}
