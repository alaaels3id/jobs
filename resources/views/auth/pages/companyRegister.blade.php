@extends('layouts.app')

@section('title', 'Register')

@section('content')
<div class="row wow fadeIn">
    <div class="col-md-12 mb-4">
        <div class="card">
            <div class="card-body">
                <section class="form-simple">
                    <div class="card">
                        <div class="header grey lighten-2">
                            <div class="text-center">
                                <h3 class="deep-red-text mb-1 mt-1 font-weight-bold">
                                    <strong>REGIST</strong><a class="deep-orange-text font-weight-bold"><strong> ER</strong></a>
                                </h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('company.register') }}">
                                @csrf
                                <div class="md-form">
                                    <input id="name" type="text" name="name" value="{{ old('name') }}" autofocus
                                        class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}">

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    <label for="Form-email4">Company Name</label>
                                </div>

                                <div class="md-form">
                                    <input id="phone" type="text" name="phone" value="{{ old('phone') }}" autofocus
                                        class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}">

                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    <label for="Form-email4">Phone</label>
                                </div>

                                <div class="md-form">
                                    <input id="address" type="text" name="address" value="{{ old('address') }}" autofocus
                                        class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}">

                                        @if ($errors->has('address'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    <label for="Form-email4">Company Address</label>
                                </div>

                                <div class="md-form">
                                    <input id="description" type="text" name="description" value="{{ old('description') }}" autofocus
                                        class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}">

                                        @if ($errors->has('description'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    <label for="Form-email4">Description</label>
                                </div>

                                <div class="md-form">
                                    <input id="Form-email4" type="email" name="email" value="{{ old('email') }}" autofocus
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    <label for="Form-email4">Your email</label>
                                </div>

                                <div class="md-form pb-3">
                                    <select name="department_id" class="form-control">
                                        <option value="0" disabled="disabled">Select a value</option>
                                        @foreach (department()->getInSelectForm() as $key => $department)
                                            <option value="{{ $key }}">{{ ucwords($department) }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="md-form pb-3">
                                    <select name="city_id" class="form-control">
                                        <option value="0" disabled="disabled">Select a value</option>
                                        @foreach (city()->getInSelectForm() as $key => $city)
                                            <option value="{{ $key }}">{{ ucwords($city) }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="md-form pb-3">
                                    <input id="password" type="password" name="password" id="Form-pass4"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    <label for="Form-pass4">Your password</label>
                                </div>

                                <div class="md-form pb-3">
                                    <input id="password_confirmation" type="password" name="password_confirmation" id="Form-pass4"
                                        class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                    <label for="Form-pass4">Confirm Password</label>
                                </div>

                                <div class="text-center mb-4">
                                    <button type="submit" class="btn btn-deep-orange btn-block z-depth-2">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection