@extends('layouts.app')
@section('title','Contact Us')
@section('style')
<style>
    .back-g { 
        background-image:url('{{ asset('front/img/img_bg_1.jpg') }}'); 
        background-repeat: no-repeat; 
        background-size:cover; 
    }
    #contact-form{padding-left:50px;padding-right:50px;}
</style>
@stop
@section('content')
<!--Section: Contact v.2-->

@if(Session::has('success'))<div class="alert alert-success">{{ Session::get('success') }}</div>@endif
<section class="view full-page-intro back-g">
    <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold text-center my-4 white-text" style="padding-top:50px">Contact us</h2>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5 white-text">
        Do you have any questions? Please do not hesitate to contact us directly.<br>
        Our team will come back to you within
        a matter of hours to help you.
    </p>

    <div class="row">
        <div class="col-md-9 mb-md-0 mb-5">
            <form action="{{ url('/contact') }}" method="POST" id="contact-form" name="contact-form">
                @csrf
                <div class="row">

                    <div class="col-md-6 ">
                        <div class="md-form mb-0 col-form-label-lg">
                            <input type="text" id="name" name="name" class="form-control">
                            <label for="name" class="text-danger">Your name</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="md-form mb-0 col-form-label-lg">
                            <input type="text" id="email" name="email" class="form-control">
                            <label for="email" class="text-danger">Your email</label>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form mb-0 col-form-label-lg">
                            <input type="text" id="subject" name="subject" class="form-control">
                            <label for="subject" class="text-danger">Subject</label>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">
                        <div class="md-form col-form-label-lg" >
                            <textarea type="text" id="message" name="message" rows="1" class="form-control md-textarea"></textarea>
                            <label for="message" class="text-danger">Your message</label>
                        </div>
                    </div>
                  
                </div>

            </form>

            <div class="text-center text-md-center">
                <a class="btn  btn-danger" onclick="document.getElementById('contact-form').submit();" 
                style="margin-top:20px;">
                 Send</a>
            </div>
            <div class="status"></div>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-3 text-center">
            <ul class="list-unstyled mb-0">
                <li><i class="fa fa-map-marker mt-4 fa-2x text-danger"></i>
                    <p class="white-text">Mansoura , CA 94126, Cairo-Egypt</p>
                </li>

                <li><i class="fa fa-phone mt-4 fa-2x text-danger"></i>
                    <p class="white-text">+ 01 234 567 89</p>
                </li>

                <li><i class="fa fa-envelope mt-4 fa-2x text-danger"></i>
                    <p class="white-text">contact@gmail.com</p>
                </li>
            </ul>
        </div>
        <!--Grid column-->

    </div>

</section>
<!--Section: Contact v.2-->
@stop