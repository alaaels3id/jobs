<!-- Scripts -->
{{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

<!-- Styles -->
{{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}

<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Bootstrap core CSS -->
<link href="{{ asset('front/css/bootstrap.min.css') }}" rel="stylesheet">

<!-- Material Design Bootstrap -->
<link href="{{ asset('front/css/mdb.min.css') }}" rel="stylesheet">

<!-- Your custom styles (optional) -->
<link href="{{ asset('front/css/style.min.css') }}" rel="stylesheet">

<style type="text/css">
    .background-image{
        background-image:url('{{ asset("front/img/img(142).jpg") }}');
        background-repeat: no-repeat; 
        background-size: cover;
    }
    @media (min-width: 800px) and (max-width: 850px) {
        .navbar:not(.top-nav-collapse) {
            background: #1C2331!important;
        }
    }
</style>