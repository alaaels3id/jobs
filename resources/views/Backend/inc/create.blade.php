@extends('backend.layouts.app')

@section('title', ucwords(str()->plural($model[0])))

@section('content')
    <section class="content-header">
        <?php $_models = str()->plural($model[0]); ?>
        <h1>Dashboard<small>Control panel</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>
                <a href="{{ route($_models.'.index') }}">
                    <i class="ion {{ $model[1] }}"></i>{{ ucwords($_models) }}
                </a>
            </li>
            <li class="active">add</li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-info">
            <div class="box-header">Add new {{ $model[0] }}</div>
            {!! 
                Form::open([
                    'url' => url('/admin-panel/'.str()->plural($model[0])), 
                    'method' => 'POST', 'class' => 'form-horizontal push-10-t '.str()->plural($model[0]).' ajax create', 'files' => true
                ]) 
            !!}
            <div class="box-body">
                @include('Backend.partials.flash')
                <div class="block block-rounded">
                    <div class="block-header bg-smooth-dark col-md-10 col-md-offset-1">

                        @if($model[2] != null)
                            @include('Backend.'.str()->plural($model[0]).'.form', ['list' => $model[2]])
                        @else
                            @include('Backend.'.str()->plural($model[0]).'.form')
                        @endif

                    </div>
                </div>
            </div>
            <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary" value="Save">
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@stop