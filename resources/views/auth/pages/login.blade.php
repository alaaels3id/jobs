@extends('layouts.app')

@section('title', 'Login')

@section('content')
<div class="row wow fadeIn">
    <div class="col-md-6 mb-4 white-text text-center text-md-left">
        <h1 class="  font-weight-bold">Computer Technology Jobs </h1>
        <hr class="hr-light">
        <p>
            <strong>Best & free guide will found here </strong>
        </p>

        <p class="mb-4 d-none d-md-block">
            <strong>
                The most comprehensive tutorial for the Bootstrap 4. Loved by over 500 000 users. Video and written versions
                available. Create your own, stunning website.
            </strong>
        </p>

        <a href="{{ route('register') }}" class="btn btn-deep-orange btn-lg"> Sign Up
            <i class="fa fa-graduation-cap ml-2"></i>
        </a>
    </div>

    <div class="col-md-6 col-xl-5 mb-4">
        <!--Card-->
        <div class="card">
            <div class="card-body">
                <section class="form-simple">
                    <div class="card">
                        <div class="header pt-3 grey lighten-2">
                            <div class="text-center">
                                <h3 class="deep-red-text mb-5 mt-4 font-weight-bold">
                                    <strong>LOG</strong>
                                    <a class="deep-orange-text font-weight-bold">
                                        <strong> IN</strong>
                                    </a>
                                </h3>
                            </div>
                        </div>
                        <div class="card-body mx-4 mt-4">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="md-form">
                                    <input id="Form-email4" type="email" name="email" value="{{ old('email') }}" autofocus
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    <label for="Form-email4">Your email</label>
                                </div>

                                <div class="md-form pb-3">
                                    <input id="password" type="password" name="password" id="Form-pass4"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    <label for="Form-pass4">Your password</label>
                                </div>

                                <div class="text-center mb-4">
                                    <button type="submit" class="btn btn-deep-orange btn-block z-depth-2">Log in</button>
                                </div>

                                <p class="font-small grey-text d-flex justify-content-center">
                                    Don't have an account?
                                    <a href="{{ route('register') }}" class="dark-grey-text font-weight-bold ml-1"> Sign up</a>
                                </p>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection