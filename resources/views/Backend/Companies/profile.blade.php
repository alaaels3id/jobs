@extends('backend.layouts.app')

@section('title', 'Doctor Profile')

@section('content')

<section class="content-header">
    <h1 class="pull-left hidden-xs">Doctor Profile</h1>
    <ol class="breadcrumb">
        <li class="h4"><a href="{{ route('doctor-panel') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active h4">Doctor Profile</li>
    </ol>
</section>

<br><br>

<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">About Me</h3>
                </div>

                <div class="box-body">
                    <img class="profile-user-img img-responsive img-circle" src="{{ getImage('user', null) }}" alt="Doctor profile picture">
                    <h3 class="profile-username text-center">{{ $doctor->name }}</h3>
                    <p class="text-muted lead text-center">Software Engineer</p>

                    <hr>

                    <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                    <p class="text-muted lead">{{ $doctor->email }}</p>

                    <hr>
                    <strong><i class="fa fa-clock-o margin-r-5"></i> Join in</strong>

                    <p class="text-muted lead">{{ $doctor->created_at->diffForHumans() }}</p>

                    <hr>

                    <strong><i class="fa fa-calendar-o margin-r-5"></i> Date Of Birth</strong>
                    <p class="text-muted lead">20-09-2019</p>

                    <hr>

                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                </div>
            </div>
        </div>
        
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">About Me</h3>
                </div>

                <div class="box-body">
                    <img class="profile-user-img img-responsive img-circle" src="{{ getImage('user', null) }}" alt="Doctor profile picture">
                    <h3 class="profile-username text-center">{{ $doctor->name }}</h3>
                    <p class="text-muted lead text-center">Software Engineer</p>

                    <hr>

                    <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                    <p class="text-muted lead">{{ $doctor->email }}</p>

                    <hr>
                    <strong><i class="fa fa-clock-o margin-r-5"></i> Join in</strong>

                    <p class="text-muted lead">{{ $doctor->created_at->diffForHumans() }}</p>

                    <hr>

                    <strong><i class="fa fa-calendar-o margin-r-5"></i> Date Of Birth</strong>
                    <p class="text-muted lead">20-09-2019</p>

                    <hr>

                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@stop