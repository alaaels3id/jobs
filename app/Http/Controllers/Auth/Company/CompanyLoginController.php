<?php

namespace App\Http\Controllers\Auth\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class CompanyLoginController extends Controller
{
    use AuthenticatesUsers;
    
	public function __construct()
	{
		$this->middleware('guest:company')->except('companyLogout');
	}

    public function showCompanyLoginForm()
    {
        return view('auth.pages.companyLogin');
    }

    public function companyLogin(Request $request)
    {
        $validation['email']    = 'required|email';
        $validation['password'] = 'required|string';

        $credentials['email']    = $request->email;
        $credentials['password'] = $request->password;
        $credentials['status']   = 1;
        
        $this->validate($request, $validation);

        if (Auth::guard('company')->attempt($credentials)) {

            return redirect()->intended('/company-panel');
        }
        return back()->withInput($request->only('email'));
    }

    public function companyLogout()
    {
        Auth::guard('company')->logout();

        // in case of multi auth, comment this two lines
        // request()->session()->flush();
        // request()->session()->regenerate();
 
        return redirect('/company-panel');
    }
}
