<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditSettingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'key'   => 'required|string',
            'value' => 'required|string',
        ];

        return $rules;
    }
}
