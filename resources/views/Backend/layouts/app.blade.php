<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @php 
        date_default_timezone_set('Africa/Cairo');
    @endphp

    <meta name="http-root" content="{{ url(Request::root()) }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <script> window.laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!} </script>
    
    <title>CTJ | @yield('title')</title>
    
    @include('includes.backend.head')

    @yield('styles')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
    
    <div class="wrapper" id="app">
        
        @include('includes.backend.header')
  
        <!-- Left side column. contains the logo and sidebar -->
        @include('includes.backend.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>
        
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="pull-right hidden-xs"><b>Version</b> 2.4.0</div>
            <strong>Copyright &copy; 2014-{{ date('Y') }} <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights reserved.
        </footer>

        <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
     </div>
    <!-- ./wrapper -->
    
    @include('includes.backend.scripts')

    @yield('scripts')
</body>
</html>