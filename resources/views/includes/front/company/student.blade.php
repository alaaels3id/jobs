@extends('layouts.app')

@section('title', 'Applyer Profile')

@section('style')
<style>
	.bg-navy  { background-color: #374258; color:#fff;  }
	.bg-white { background-color: #fff; color:#374258;  }
</style>
@stop

@section('content')
@if(Session::has('success')) <div class="alert alert-success">{{ Session::get('success') }}</div> @endif
<div class="row wow fadeIn">
	<div class="col-md-8">
		<div class="card bg-navy">
			<div class="card-header">Info</div>
			<div class="card-body bg-white">
				<ul class="list-group">
					<li class="list-group-item">
						<img src="{{ getImage('user', $user->image) }}" width="60" height="60" class="rounded-circle" alt="">
					</li>
					<li class="list-group-item">Name: {{ $user->fullname }}</li>
					<li class="list-group-item">Email: {{ $user->email }}</li>
					<li class="list-group-item">Address: {!! getDeafultValue($user->address) !!}</li>
					<li class="list-group-item">Phone: {!! getDeafultValue($user->phone) !!}</li>
					<li class="list-group-item">Department: {{ $user->department->name }}</li>
					<li class="list-group-item">City: {{ $user->city->name }}</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card bg-navy">
			<div class="card-header">Courses ({{ getUserCourses($user->id)->count() }})</div>
			<div class="card-body bg-white">
				<ul class="list-group">
					@forelse(getUserCourses($user->id) as $course)
						<li class="list-group-item">{{ $course->name }}</li>
					@empty
						<li class="list-group-item">No Cources</li>
					@endforelse
				</ul>
			</div>
		</div>
		<div class="card bg-navy">
			<div class="card-header">Applyed Jobs ({{ getUserApplyedJobs($user->id)->count() }})</div>
			<div class="card-body bg-white">
				<ul class="list-group">
					@forelse(getUserApplyedJobs($user->id) as $job)
						<li class="list-group-item">{{ $job->jop->name }}</li>
					@empty
						<li class="list-group-item">No Jobs</li>
					@endforelse
				</ul>
			</div>
		</div>
	</div>
</div>
@stop