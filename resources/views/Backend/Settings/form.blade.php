<div class="form-group">
	@include('includes.backend.inputs', ['errors' => $errors, 'type' => 'text', 'name' => 'key', 'style' => 'form-control form-data','col'=>'6'])
	@include('includes.backend.inputs', ['errors' => $errors, 'type' => 'text', 'name' => 'value', 'style' => 'form-control form-data','col'=>'6'])
</div>