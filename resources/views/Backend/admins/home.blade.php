@extends('backend.layouts.app')

@section('title', 'Home')

@section('styles')
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('admin/bower_components/jvectormap/jquery-jvectormap.css') }}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ asset('admin/bower_components/morris.js/morris.css') }}">
@stop

@section('content')

    <section class="content-header">
        <h1>Dashboard<small>Control panel</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">        
        <!-- Main row -->
        <div class="row">
            <div class="col-md-3">
                @foreach(models() as $model)
                    <!-- small box -->
                    <div class="small-box {{ $model[2] }}">
                        <div class="inner">
                            <h3>{{ getModelCount(ucfirst($model[0])) }}</h3>
                            <p>{{ ucwords(str()->plural($model[0])) }}</p>
                        </div>
                        <div class="icon"><i class="ion {{ $model[1] }}"></i></div>
                        <a href="{{ route(str()->plural($model[0]).'.index') }}" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>            
                @endforeach
            </div>
            <!-- Left col -->
            <section class="col-lg-9 connectedSortable">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Jops</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin text-center">
                                <thead>
                                    <tr>
                                        <th>Job ID</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Salary</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    @forelse($data['jops'] as $jop)
                                    <tr>
                                        <td><a href="javascript:void();">#{{ $jop->id }}</a></td>
                                        <td>{{ ucwords($jop->name) }}</td>
                                        <td>{{ limit($jop->description, 30) }}</td>
                                        <td>{{ $jop->salary }}</td>
                                        <td>
                                            @if($jop->status == 1)<span class="label label-success">Active</span>
                                            @else<span class="label label-danger">Disactive</span>
                                            @endif
                                        </td>
                                    </tr>
                                    @empty
                                    <tr><td colspan="5">No Data</td></tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="box-footer clearfix">
                        <a type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-jops" href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">New Job</a>
                        <a href="{{ route('jops.index') }}" class="btn btn-sm btn-primary btn-flat pull-right">View All Jobs</a>
                    </div>
                </div>
                
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Companies</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin text-center">
                                <thead>
                                    <tr>
                                        <th>Comapny ID</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Department</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    @forelse($data['companies'] as $company)
                                    <tr>
                                        <td><a href="javascript:void();">#{{ $company->id }}</a></td>
                                        <td>{{ ucwords($company->name) }}</td>
                                        <td>{!! getDeafultValue($company->address) !!}</td>
                                        <td>{{ ucwords($company->department->name) }}</td>
                                        <td>
                                            @if($company->status == 1)<span class="label label-success">Active</span>
                                            @else<span class="label label-danger">Disactive</span>
                                            @endif
                                        </td>
                                    </tr>
                                    @empty
                                    <tr><td colspan="5">No Data</td></tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="box-footer clearfix">
                        <a type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-companies" href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">New Company</a>
                        <a href="{{ route('companies.index') }}" class="btn btn-sm btn-primary btn-flat pull-right">View All Companies</a>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xs-6">
                        @include('includes.front.table', ['data' => $data['users'], 'model' => 'user', 'color' => 'danger'])
                        @include('includes.front.table', ['data' => $data['admins'], 'model' => 'admin', 'color' => 'primary'])
                    </div>
                    <div class="col-xs-6">
                        {{-- @include('includes.front.table', ['data' => $data['admins'], 'model' => 'admin', 'color' => 'primary']) --}}

                        @include('includes.front.table', ['data' => $data['companies'], 'model' => 'company', 'color' => 'info'])
                    </div>
                </div>
            </section>
        
        </div>
        <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
    <div class="modal fade" id="modal-jops">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add New Jop</h4>
                </div>
                {!! Form::open(['url' => url('/admin-panel/jops'), 'method' => 'POST', 'class' => 'ajax edit']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-xs-12 {{ $errors->first('name' , 'has-error') }}">
                                <div class="form-valid floating">
                                    <label for="name">Name</label>
                                    {!! Form::text('name', null, ['class'=>'form-data form-control','id' => 'name']) !!}
                                    
                                    {!! 
                                        $errors->first('name','<div class="help-block text-right animated fadeInDown val-error">:message</div>')
                                    !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12 {{ $errors->first('description' , 'has-error') }}">
                                <div class="form-valid floating">
                                    <label for="description">Description</label>
                                    {!! Form::text('description', null, ['class'=>'form-data form-control','id' => 'description']) !!}
                                    
                                    {!! 
                                        $errors->first('description','<div class="help-block text-right animated fadeInDown val-error">:message</div>')
                                    !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12 {{ $errors->first('salary' , 'has-error') }}">
                                <div class="form-valid floating">
                                    <label for="salary">Salary</label>
                                    {!! Form::number('salary', null, ['class'=>'form-data form-control','id' => 'salary']) !!}
                                    
                                    {!! 
                                        $errors->first('salary','<div class="help-block text-right animated fadeInDown val-error">:message</div>')
                                    !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-companies">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add New Company</h4>
                </div>
                {!! Form::open(['url' => url('/admin-panel/companies'), 'method' => 'POST', 'class' => 'ajax edit']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            @include('includes.inputs', [
                                'errors' => $errors, 'type' => 'text', 'name' => 'name', 
                                'style' => 'form-control form-data', 'col' => '12'
                            ])
                        </div>

                        <div class="form-group">
                            @include('includes.inputs', [
                                'errors' => $errors, 'type' => 'email', 'name' => 'email', 
                                'style' => 'form-control form-data', 'col' => '12'
                            ])
                        </div>

                        <div class="form-group">
                            @include('includes.inputs', [
                                'errors' => $errors, 'type' => 'select', 
                                'name' => 'city_id', 'style' => 'form-control form-data',
                                'list' => $list['cities'], 'col' => '12'
                            ])
                            @include('includes.inputs', [
                                'errors' => $errors, 'type' => 'select', 
                                'name' => 'department_id', 'style' => 'form-control form-data',
                                'list' => $list['departments'], 'col' => '12'
                            ])
                        </div>

                        <div class="form-group">
                            @include('includes.inputs', [
                                'errors' => $errors, 'type' => 'text', 'name' => 'phone', 
                                'style' => 'form-control form-data', 'col' => '12'
                            ])
                        </div>

                        <div class="form-group">
                            @include('includes.inputs', [
                                'errors'=>$errors,'type'=>'password','name'=>'password',
                                'style'=>'form-control form-data','col'=>'6'
                            ])
                            @include('includes.inputs',[
                                'errors'=>$errors,'type'=>'password_confirmation','name'=>'password',
                                'style'=>'form-control form-data','col'=>'6'
                            ])
                        </div>

                        <div class="form-group">
                            @include('includes.inputs', [
                                'errors' => $errors, 'type' => 'text', 'name' => 'description', 
                                'style' => 'form-control form-data', 'col' => '12'
                            ])
                        </div>
                        <div class="form-group">
                            @include('includes.inputs', [
                                'errors' => $errors, 'type' => 'text', 'name' => 'address', 
                                'style' => 'form-control form-data', 'col' => '12'
                            ])
                        </div>
                    </div>
                </div>
                <br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('admin/dist/js/pages/dashboard.js') }}"></script>
@stop