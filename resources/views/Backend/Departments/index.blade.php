@extends('backend.layouts.app')

@section('title', 'Departments')

@section('styles')
	<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@stop

@section('content')
    
    <section class="content-header">
        <h1>Dashboard<small>Control panel</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Departments</li>
        </ol>
    </section>

    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box">
    				<div class="box-header">
    					<h3 class="box-title">Data Table With Full Features</h3>
    				</div>
    				<!-- /.box-header -->
    				<div class="box-body">
    					<table id="departments" class="table table-bordered table-striped text-center">
    						<thead>
    							<tr>
    								<th>#</th>
				                  	<th>Name</th>
				                  	<th>Status</th>
				                  	<th>Actions</th>
				                </tr>
	                		</thead>
	                		<tbody data-count={{ $departments->count() }}>
			                	@forelse($departments as $key => $department)
				                <tr id="department-row-{{ $department->id }}">
				                	<td>{{ (int)$key + 1 }}</td>
				                	<td>{{ ucwords($department->name) }}</td>
				                	<td>
				                		@if($department->status == 1)
				                			<label class="switch" id="checkbox-id-{{ $department->id }}">
				                				<input type="checkbox" class="form-control" id="#active-id-{{ $department->id }}" onclick="isChecked('checked', '{{ $department->id }}');" value="1" checked><span class="slider round"></span>
				                			</label>
				                		@else
				                			<label class="switch">
				                				<input type="checkbox" class="form-control" id="#active-id-{{ $department->id }}" onclick="isChecked('null', '{{ $department->id }}');" value="0"><span class="slider round"></span>
				                			</label>
				                		@endif
				                	</td>
				                	<td>
				                		<ul class="list-unstyled list-inline">
				                			<li>
				                				<a href="{{ route('departments.create') }}" data-toggle='tooltip' title="Add" class="btn btn-primary btn-sm">
				                					<i class="fa fa-plus"></i>
				                				</a>
				                			</li>
				                			<li>
				                				<a href="{{ route('departments.edit', ['id' => $department->id]) }}" data-toggle='tooltip' title="Edit" class="btn btn-success btn-sm">
				                					<i class="fa fa-edit"></i>
				                				</a>
				                			</li>
				                			<li>
				                				<a 
				                					href='{{ url('/departments/'.$department->id) }}' 
				                					
				                					class='btn btn-sm btn-danger delete-action' 
				                					
				                					type='button'

				                					data-id="{{ $department->id }}" 
				                					
				                					data-toggle='tooltip' 
													
													title='Delete'><i class='fa fa-times'></i>
												</a>
				                			</li>
				                		</ul>
				                	</td>
				                </tr>
			                	@endforeach
	                		</tbody>
	                		<tfoot>
	                			<tr>
    								<th>#</th>
				                  	<th>Name</th>
				                  	<th>Status</th>
				                  	<th>Actions</th>
	                			</tr>
	                		</tfoot>
	              		</table>
	            	</div>
	            	<!-- /.box-body -->
	          	</div>
	          	<!-- /.box -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@stop

@section('scripts')
	<script src="{{ asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script>
	  	
	  	function isChecked(value, id)
		{
	        $.ajax({
	            type: 'POST',
	            url: 'ajax-change-department-status',
	            data: { id: id, value: value },
	            success: function(response) 
	            {
					var dialog = bootbox.dialog({
					    message: '<p class="text-center">'+response.message+'</p>',
					    closeButton: false
					});
	                setTimeout(function(){ dialog.modal('hide'); }, 1000);
	            },
	            error: function(x) { crud_handle_server_errors(x); },
	            complete:function(){ window.location.reload(); }
	        });
		}

		$('.delete-action').on('click', function(e)
		{
			var id = $(this).data('id');
			e.preventDefault();
	        
	        bootbox.confirm({
	            title: 'Confirmation Message',
	            size: "small",
	            message: 'Are you sure you want to remove this department',
	            buttons: {
	                cancel: {
	                    label: '<i class="fa fa-times"></i> Cancel'
	                },
	                confirm: {
	                    label: '<i class="fa fa-check"></i> Confirm',
	                    className: "btn-danger"
	                }
	            },
	            callback: function(result) 
	            {
	                if (result) 
	                {
						$.ajax({
							type: 'POST',
							url: 'departments/ajax-delete-department',
							data: {id: id},
							success: function(response){$('#department-row-'+id).fadeOut(); },
							error: function(x) { crud_handle_server_errors(x); },
							complete: function()
							{
								var tbody = $('table#departments tbody');
								var count = tbody.data('count');
								if(count == 1) tbody.append(`<tr><td colspan="5"><strong>No Trashed</strong></td></tr>`);
							}
						});
	                }
	            }
	        });
		});

		$(function () {
			$('#departments').DataTable({
	      		'paging'      : true,
	      		'lengthChange': true,
	      		'searching'   : true,
			    'ordering'    : true,
			    'info'        : true,
			    'autoWidth'   : true
	    	})
	  	});

	</script>
@stop