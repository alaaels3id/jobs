<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JopUser extends Model
{
    protected $guarded = ['id'];

    public function jop()
    {
        return $this->belongsTo('App\Models\Jop', 'jop_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public static function createUserJop($userJopData)
    {
        $createdCompanyJop = JopUser::updateOrcreate(['user_id'=>$userJopData['user_id'],'jop_id'=>$userJopData['jop_id']]);
        return $createdCompanyJop;
    }

    public static function updateUserJop($userJopData, $currentUserJop)
    {
        $updateUserJop = $currentUserJop->update($userJopData);

        return $updateUserJop;
    }
}
