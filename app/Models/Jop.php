<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hash, DB, Image, Auth;

class Jop extends Model
{
	use SoftDeletes;
	
    protected $guarded = ['id'];

    protected $casts = ['status' => 'boolean'];

    public function scopeStatus($query, $parm)
    {
        return $query->where('status', $parm);
    }

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public static function uploadImage($img)
    {
        $filename = 'jop_' . str_random(12) . '_' . date('Y-m-d') . '.' . strtolower($img->getClientOriginalExtension());

        if (!file_exists(public_path('uploaded/jops/')))
            mkdir(public_path('uploaded/jops/'), 0777, true);

        $path = public_path('uploaded/jops/');
        
        $img = Image::make($img)->save($path . $filename);

        return $filename;
    }

    public static function createJop($jopData) 
    {
        if (request()->hasFile('image')) $jopData['image'] = Jop::uploadImage($jopData['image']);
        
        else $jopData = array_except($jopData, ['image']);
        
        $createdJop = Jop::updateOrcreate(array_except($jopData, ['_token']));

        return $createdJop;
    }

    public static function updateJop($jopData, $currentJop)
    {
        if (request()->hasFile('image')) $jopData['image'] = Jop::uploadImage($jopData['image']);
        
        else $jopData = array_except($jopData, ['image']);

        $updateJop = $currentJop->update($jopData);

        return $updateJop;
    }

    public static function updateJopStatus($jopData, $currentJop)
    {
        $value = $jopData['value'];

        // if the value comes with checked that mean we want the reverse value of it;
        return ($value == 'checked') ? $currentJop->update(['status' => 0]) : $currentJop->update(['status' => 1]);
    }

    public static function getInSelectForm($with_main = null , $with_null = null, $exceptedIds = [] )
    {
        $jops   = [];

        $with_null == null ? $jops = $jops : $jops = $jops + [''  => ''];
        $with_main == null ? $jops = $jops : $jops = $jops + ['0' => 'Main'];

        $jopsDB = Jop::whereNotIn('id',$exceptedIds)->get();

        foreach ($jopsDB as $jop) { $jops[$jop->id] = ucwords($jop->name); }

      	return $jops;
    }
}
