@extends('layouts.app')

@section('title', 'Register')

@section('content')
<div class="row wow fadeIn">
    <div class="col-md-12 mb-4">
        <div class="card">
            <div class="card-body">
                <section class="form-simple">
                    <div class="card">
                        <div class="header grey lighten-2">
                            <div class="text-center">
                                <h3 class="deep-red-text mb-1 mt-1 font-weight-bold">
                                    <strong>REGIST</strong><a class="deep-orange-text font-weight-bold"><strong> ER</strong></a>
                                </h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="md-form">
                                    <input id="first_name" type="text" name="first_name" value="{{ old('first_name') }}" autofocus
                                        class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}">

                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    <label for="Form-email4">First Name</label>
                                </div>

                                <div class="md-form">
                                    <input id="last_name" type="text" name="last_name" value="{{ old('last_name') }}" autofocus
                                        class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}">

                                        @if ($errors->has('last_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    <label for="Form-email4">Last Name</label>
                                </div>

                                <div class="md-form">
                                    <input id="Form-email4" type="email" name="email" value="{{ old('email') }}" autofocus
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    <label for="Form-email4">Your email</label>
                                </div>

                                <div class="md-form pb-3">
                                    <select name="department_id" class="form-control">
                                        <option value="0" disabled="disabled">Select a value</option>
                                        @foreach (App\Models\Department::getInSelectForm() as $key => $department)
                                            <option value="{{ $key }}">{{ ucwords($department) }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="md-form pb-3">
                                    <select name="city_id" class="form-control">
                                        <option value="0" disabled="disabled">Select a value</option>
                                        @foreach (App\Models\City::getInSelectForm() as $key => $city)
                                            <option value="{{ $key }}">{{ ucwords($city) }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="md-form pb-3">
                                    <input id="password" type="password" name="password" id="Form-pass4"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    <label for="Form-pass4">Your password</label>
                                </div>

                                <div class="md-form pb-3">
                                    <input id="password_confirmation" type="password" name="password_confirmation" id="Form-pass4"
                                        class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                    <label for="Form-pass4">Confirm Password</label>
                                </div>

                                <div class="text-center mb-4">
                                    <button type="submit" class="btn btn-deep-orange btn-block z-depth-2">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
 @endsection
