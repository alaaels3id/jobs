@extends('backend.layouts.app')

@section('title', 'Admin Profile')

@section('content')

<section class="content-header">
    <h1 class="pull-left hidden-xs">Admin Profile</h1>
    <ol class="breadcrumb">
        <li class="h4"><a href="{{ route('admin-panel') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active h4">Admin Profile</li>
    </ol>
</section>

<br><br>

<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">About Me</h3>
                </div>

                <div class="box-body">
                    <img class="profile-user-img img-responsive img-circle" src="{{ getImage('admin', $auth->image) }}" alt="Admin profile picture">
                    <h3 class="profile-username text-center">{{ $admin->name }}</h3>
                    <p class="text-muted lead text-center">Software Engineer</p>

                    <hr>

                    <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                    <p class="text-muted lead">{{ $admin->email }}</p>

                    <hr>
                    <strong><i class="fa fa-clock-o margin-r-5"></i> Join in</strong>

                    <p class="text-muted lead">{{ $admin->created_at->diffForHumans() }}</p>

                    <hr>

                    <strong><i class="fa fa-calendar-o margin-r-5"></i> Date Of Birth</strong>
                    <p class="text-muted lead">20-09-2019</p>

                    <hr>

                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Last Update</strong>

                    <p class="text-muted lead">{{ $auth->updated_at->diffForHumans() }}</p>
                </div>
            </div>
        </div>
        
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Settings</h3>
                </div>

                <div class="box-body">
                    {!! Form::model($auth,['url'=>route('admins.updateprofile',['id'=>$auth->id]),'class'=>'ajax edit','method'=>'POST','files'=>true]) !!}
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        {!! Form::text('first_name', null, ['class'=>'form-control form-data','id'=>'first_name']) !!}
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        {!! Form::text('last_name', null, ['class'=>'form-control form-data','id'=>'last_name']) !!}
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        {!! Form::text('email', null, ['class'=>'form-control form-data','id'=>'email']) !!}
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        {!! Form::text('address', null, ['class'=>'form-control form-data','id'=>'address']) !!}
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        {!! Form::text('phone', null, ['class'=>'form-control form-data','id'=>'phone']) !!}
                    </div>
                    <div class="form-group {{ $errors->first('image' , 'has-error') }}">
                        <div class="row img-media">
                            <div class="col-xs-12">
                                <div class="form-valid">
                                    <label for="image">Image</label>
                                    <input type="file" class="form-data form-control" id="image" accept="image/*" name="image">
                                    {!! $errors->first('image', '<div class="help-block text-right animated fadeInDown val-error">:message</div>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::submit('Save', ['class'=>'form-control btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop