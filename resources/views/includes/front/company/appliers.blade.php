@extends('layouts.app')

@section('title', 'Jobs Appliers')

@section('style')
<style>
	.bg-navy  { background-color: #374258; color:#fff;  }
	.bg-white { background-color: #fff; color:#374258;  }
	.company-image { border-radius: 50%; padding: 5px; margin: 5px; display: inline-block;}
</style>
@stop

@section('content')
<div class="row wow fadeIn">
	<div class="col-md-8">
		@forelse($appliers as $applier)
		<br>
		<div class="card bg-navy">
			<div class="card-header">#{{ ucwords($applier->jop->name) }}</div>
			<div class="card-body bg-white">
				<img src="{{ getImage('user',$applier->user->image) }}" class="company-image" width="60" height="60" alt="">
				<b>User Name</b> :
                <a href="{{ route('company.user-profile',['id'=>$applier->user->id]) }}">
                    {{ ucwords($applier->user->fullname) }}    
                </a>
                <br>
				<b>Job Name</b> : {{ ucwords($applier->jop->name) }} <br>
				<b>Job Description</b> : {{ ucwords($applier->jop->description) }} <br>
				<b>Since</b> : {{ $applier->created_at->diffForHumans() }} <br>
			</div>
		</div>
		<br>
		@empty
		<div class="card bg-navy">
			<div class="card-header">Recommended Jobs For You</div>
			<div class="card-body bg-white">No JOBS</div>
		</div>
		@endforelse
	</div>
	<div class="col-md-4">
        <br>
        @guest()
        <div class="card mb-4 text-center wow fadeIn">
            <div class="card-header">Do you want to get informed about new articles?</div>
            <div class="card-body">
                <form>
                    <label for="defaultFormEmailEx" class="grey-text">Your email</label>
                    <input type="email" id="defaultFormLoginEmailEx" class="form-control">
                    <br>
                    <label for="defaultFormNameEx" class="grey-text">Your name</label>
                    <input type="text" id="defaultFormNameEx" class="form-control">
                    <div class="text-center mt-4">
                        <button class="btn btn-danger btn-md" type="submit">Sign up</button>
                    </div>
                </form>
            </div>
        </div>
        @else
        <div class="card mb-4 text-center wow fadeIn">
            <div class="card-header text-danger"> Your Profile Info </div>
            <div class="card-body">
                <p><b>Name :</b>{{ ucwords($auth->fullname) }}</p>
                <p><b>E-mail:</b>{{ $auth->email }}</p>
                <p><b>Since:</b> {{ $auth->created_at->format('d-m-Y') }}</p>
                <p><b>Department:</b> {{ $auth->department->name }}</p>
                <div class="text-center mt-4">
                    @if (auth()->user())
                        @if(auth()->user()->guard == 'web')
                            <a href="{{ route('users.profile',['id'=>$auth->id]) }}" class="btn btn-danger btn-md">Update</a>
                        @else
                            <a href="{{ route('company.profile',['id'=>$auth->id]) }}" class="btn btn-danger btn-md">Update</a>
                        @endif
                    @endif
                </div>
            </div>
        </div>
        @endguest
	</div>
</div>
@stop