<header class="main-header">
    <a href="{{ route('admin-panel') }}" class="logo">
        <span class="logo-mini"><b>C</b>TJ</span>
        <span class="logo-lg"><b>CTJ</b> Finder</span>
    </a>

    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"><span class="sr-only">Toggle navigation</span></a>
        
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope-o"></i>
                        <span class="label label-success">{{ contact()->count() }}</span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="header">You have {{ contact()->count() }} messages</li>
                        <li>
                            <ul class="menu">
                                @forelse(contact()->get() as $message)
                                @php
                                    $user = user()->where('email', $message->email)->get();
                                    $image = $user->count() > 0 ? $user->image : null;
                                @endphp
                                <li onclick="setMessageToBeSeen({{ $message->id }});">
                                    <a href="{{ route('contacts.index') }}">
                                        <div class="pull-left">
                                            <img src="{{ getImage('user', $image) }}" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>{{ $message->subject }}<small><i class="fa fa-clock-o"></i> {{ $message->created_at->diffForHumans() }}</small></h4>
                                        <p>{{ limit($message->message, 20) }}</p>
                                    </a>
                                </li>
                                @empty
                                No Messages
                                @endforelse
                            </ul>
                        </li>
                        <li class="footer"><a href="{{ route('contacts.index') }}">See All Messages</a></li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ getImage('admin', $auth->image) }}" class="user-image" alt="@isset($auth) {{ $auth->fullname }} @endisset">
                        <span class="hidden-xs">{{ ucwords($auth->fullname) }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ getImage('admin', $auth->image) }}" class="img-circle" alt="User Image">
                            <p>
                                @isset($auth) {{ ucwords($auth->fullname) }} @else UNKNOWN @endisset - is a Admininstrator
                                <small> Member since @isset($auth) {{ $auth->created_at->format('M. Y') }} @endisset </small>
                            </p>
                        </li>

                        <!-- Menu Body -->
                        <li class="user-body"></li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('admins.profile', ['id' => $auth->id]) }}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">

                                <a class="btn btn-default btn-flat" href="{{ route('admin.logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{ __('Sign out') }}
                                </a>

                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>