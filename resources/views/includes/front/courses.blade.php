@extends('layouts.app')

@section('title', 'Available Jobs')

@section('style')
<style>
	.bg-navy  { background-color: #374258; color:#fff;  }
	.bg-white { background-color: #fff; color:#374258;  }
	.company-image { border-radius: 50%; padding: 5px; margin: 5px; display: inline-block;}
</style>
@stop

@section('content')

@if(Session::has('success'))<div class="alert alert-success">{{ Session::get('success') }}</div>@endif
@if(Session::has('danger'))<div class="alert alert-danger">{{ Session::get('danger') }}</div>@endif

<div class="row wow fadeIn">
	<div class="col-md-8">
		@forelse($courses as $course)
    		<br>
    		<div class="card bg-navy">
    			<div class="card-header">#{{ ucwords($course->name) }} Course</div>
    			<div class="card-body bg-white">
    				<img src="{{ getImage('user', $course->user->image) }}" class="company-image" width="60" height="60" alt="">
    				<b>User Name</b> : {{ ucwords($course->user->fullname) }} <br>
    				<b>Course Name</b> : {{ ucwords($course->name) }} <br>
    				<b>Course Localtion</b> : {{ ucwords($course->city->name) }} <br>
    				<b>User Department</b> : {{ ucwords($course->user->department->name) }} <br>
    				<b>Since</b> : {{ $course->created_at->diffForHumans() }} <br>
    			</div>
    		</div>
    		<br>
		@empty
    		<div class="card bg-navy">
    			<div class="card-header">My Courses</div>
    			<div class="card-body bg-white">No Courses</div>
    		</div>
		@endforelse
	</div>

	<div class="col-md-4">
        <br>
        <div class="card mb-4 text-center wow fadeIn">
            <div class="card-header">Courses</div>
            <div class="card-body">
                <form action="{{ route('users.add-course',['user_id'=>$auth->id]) }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <select name="city_id" class="form-control" id="city_id">
                            @foreach (city()->where('status', 1)->get() as $city)
                                <option value="{{ $city->id }}">{{ ucwords($city->name) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="department_id" class="form-control" id="city_id">
                            @foreach (department()->where('status', 1)->get() as $department)
                                <option value="{{ $department->id }}">{{ ucwords($department->name) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="Course Name" name="name" id="defaultFormNameEx" class="form-control">
                        <input type="hidden" value="{{ $auth->id }}" name="user_id">
                    </div>
                    <div class="text-center mt-4">
                        <button class="btn btn-info btn-md" type="submit">Add Course</button>
                    </div>
                </form>
            </div>
        </div>
	</div>
</div>
@stop