<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCourseRequest;
use App\Http\Requests\EditCourseRequest;
use App\Models\Course;
use App\Models\City;
use App\Models\User;
use App\Models\Department;
use Auth, DB, Image, File, Validator, Response, Hash;

class CourseController extends Controller
{
    public function index()
    {
        $courses = Course::all();
        return view('Backend.courses.index', compact('courses'));
    }

    public function home()
    {
        return view('Backend.courses.home');
    }

    public function Trashed()
    {
        $courses = Course::onlyTrashed()->get();
        return view('Backend.courses.trashed', compact('courses'));
    }

    public function create()
    {
        $lists['cities'] = City::getInSelectForm();
        $lists['users'] = User::getInSelectForm();
        $lists['departments'] = Department::getInSelectForm();
        return view('Backend.courses.create', compact('lists'));
    }

    public function store(CreateCourseRequest $request)
    {
        $courseData = $request->all();

        if(Course::createCourse($courseData))
        {
            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Added Successfully')]);
            } else {
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Added Successfully'));
            }
        }
        else
        {
            if(request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Internal Server Error 500')]);
            } else {
                request()->session()->flash('status','danger');
                request()->session()->flash('message',__('Internal Server Error 500'));
            }

            return back()->withInputs();
        }
        return redirect()->route('courses.index');
    }

    public function ChangeCourseStatus(Request $request)
    {
        // Request ajax from index;
        $courseData = $request->all();
        $currentCourse = Course::find($request->id);
        
        if (!$updateCourseStatus = Course::updateCourseStatus($courseData, $currentCourse))
            return response()->json(['requestStatus' => false, 'message' => trans('Server Internal Error 500')]);

        return response()->json(['requestStatus' => true, 'message' => trans('Data Updated Successfully')]);
    }

    public function edit($id)
    {
        $course = Course::find($id);
        $lists['cities'] = City::getInSelectForm();
        $lists['users'] = User::getInSelectForm();
        $lists['departments'] = Department::getInSelectForm();
        return view('Backend.courses.edit', compact('course', 'lists'));
    }

    public function update(EditCourseRequest $request, $id)
    {
        $courseData = $request->all();
        $currentCourse = Course::find($id);
        
        if (Course::updateCourse($courseData, $currentCourse)) {

            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Updated Successfully')]);
            }else{
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Updated Successfully'));
            }
        } else {
            if (request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Server Internal Error 500')]);
            } else {
                 request()->session()->flash('status','danger');
                 request()->session()->flash('message',__('Server Internal Error 500'));
            }
        }

        return redirect()->route('courses.edit', ['id' => $currentCourse->id]);
    }

    public function DeleteCourse(Request $request)
    {
        $course = Course::find($request->id);

        if(!$course) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, Course is not exists !!']);

        try
        {            
            $course->delete();
            
            return response()->json(['deleteStatus' => true, 'message' => 'Course Deleted Successfully']);
        }
        catch (Exception $e){ return response()->json(['deleteStatus' => false,'error' => 'Server Internal Error 500']); }

        return redirect()->route('courses.index');
    }

    public function RestoreCourse(Request $request)
    {
        $restore = DB::table('courses')->where('id', $request->id)->where('deleted_at', '!=', null)->update(['deleted_at' => null]);
        
        $response['requestStatus']  = ($restore) ? true : false;
        $response['message']        = ($restore) ? 'Course Restored Successfully' : 'Server Internal Error 500';

        return response()->json($response);
    }

    public function RemoveCourse(Request $request)
    {
        $remove = DB::table('courses')->where('id', $request->id)->where('deleted_at', '!=', null);

        if (isset($remove->image) && $remove->image != null) File::delete('uploaded/courses/'.$remove->image);

        $remove->delete();
        
        $response['requestStatus']  = ($remove) ? true : false;

        return response()->json($response);
    }
}
