<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hash, DB, Image, Auth;

class Setting extends Model
{
	use SoftDeletes;
	
    protected $guarded = ['id'];

    protected $casts = ['status' => 'boolean'];

    public function scopeStatus($query, $parm)
    {
        return $query->where('status', $parm);
    }

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'admin_id', 'id');
    }

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public static function uploadImage($img)
    {
        $filename = 'setting_' . str_random(12) . '_' . date('Y-m-d') . '.' . strtolower($img->getClientOriginalExtension());

        if (!file_exists(public_path('uploaded/settings/')))
            mkdir(public_path('uploaded/settings/'), 0777, true);

        $path = public_path('uploaded/settings/');
        
        $img = Image::make($img)->save($path . $filename);

        return $filename;
    }

    public static function createSetting($settingData) 
    {
        if (request()->hasFile('image')) $settingData['image'] = Setting::uploadImage($settingData['image']);
        
        else $settingData = array_except($settingData, ['image']);
        
        $createdSetting = Setting::updateOrcreate(array_except($settingData, ['_token']));

        return $createdSetting;
    }

    public static function updateSetting($settingData, $currentSetting)
    {
        if (request()->hasFile('image')) $settingData['image'] = Setting::uploadImage($settingData['image']);
        
        else $settingData = array_except($settingData, ['image']);

        $updateSetting = $currentSetting->update($settingData);

        return $updateSetting;
    }

    public static function updateSettingStatus($settingData, $currentSetting)
    {
        $value = $settingData['value'];

        // if the value comes with checked that mean we want the reverse value of it;
        return ($value == 'checked') ? $currentSetting->update(['status' => 0]) : $currentSetting->update(['status' => 1]);
    }

    public static function getInSelectForm($with_main = null , $with_null = null, $exceptedIds = [] )
    {
        $settings   = [];

        $with_null == null ? $settings = $settings : $settings = $settings + [''  => ''];
        $with_main == null ? $settings = $settings : $settings = $settings + ['0' => 'Main'];

        $settingsDB = Setting::whereNotIn('id',$exceptedIds)->get();

        foreach ($settingsDB as $setting) { $settings[$setting->id] = ucwords($setting->name); }

      	return $settings;
    }
}
