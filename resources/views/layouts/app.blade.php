<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="icon" href="{{ asset('front/img/1.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Computer Technology Jobs | @yield('title')</title>

    @include('includes.front.head')
    <style>
        .back-ground { 
            background-image:url('{{ asset('front/img/img(142).jpg') }}');
            background-repeat: no-repeat; 
            background-size: cover; 
            background-attachment: fixed;
        }
    </style>
    @yield('style')
</head>
@php
    $class1 = Request::is('about') ? '' : 'mdb-color';
    $class2 = Request::is('login') || Request::is('register') || Request::is('company-panel/login') || Request::is('company-panel/register') 
            ? 'back-ground' : '';
@endphp
<body class="{{ $class1 . ' ' . $class2 }}">
    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
        <div class="container">
            <!-- Brand -->
            <a class="navbar-brand" href="{{ Request::is('company-panel/*') || Request::is('company-panel') ? url('/company-panel') : url('/') }}">
                <img src="{{ asset('front/img/1.png') }}" height="30" class="d-inline-block align-top" alt="logo"> 
            </a>
            <!-- Collapse -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" 
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Links -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item "><a class="nav-link" href="{{ Request::is('company-panel/*') || Request::is('company-panel') ? url('/company-panel') : url('/') }}">Home</a></li>

                    @isset($auth)
                        @if(Auth::user()->guard != 'company')
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" 
                                data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">For Student
                            </a>
                            <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('users.jops') }}">Available Jobs</a>
                                <a class="dropdown-item" href="{{ route('users.courses',['id'=>$auth->id]) }}">My Courses</a>
                                <a class="dropdown-item" href="{{ route('users.my-jobs') }}">My Jobs</a>
                                <a class="dropdown-item" href="{{ route('users.profile',['id'=>$auth->id]) }}">My Profile</a>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('about') }}">About Us</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('contact') }}">Contact</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('users.companies') }}">Companies</a></li>

                        @endif
                        @if(auth()->user()->guard == 'company')
                            <li class="nav-item"><a class="nav-link" href="{{ route('company.appliers',['id'=>$auth->id]) }}">Appliers</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('company.jobs') }}">Jobs</a></li>
                        @endif
                    @endisset

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink1" 
                            data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">For Company
                        </a>
                        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                            @forelse(getUserCompanies() as $company)
                            @isset ($auth)
                                @if (auth()->user()->guard == 'web')
                                    <a class="dropdown-item" href="{{ route('users.company',['id'=>$company->id]) }}">
                                        {{ ucwords($company->name) }}
                                    </a>
                                @else
                                    <a class="dropdown-item" href="{{ route('company.profile',['id'=>$company->id]) }}">
                                        {{ ucwords($company->name) }}
                                    </a>
                                @endif
                            @endisset
                            @empty
                            <a href="javascript:void(0)">No Companies</a>
                            @endforelse
                        </div>
                    </li>
                </ul>

                <!-- Right -->
                <ul class="navbar-nav nav-flex-icons">
                   @guest
                   <li class="nav-item">
                        <a href="https://www.facebook.com/" class="nav-link" target="_blank"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="nav-item">
                        <a href="https://twitter.com/" class="nav-link" target="_blank"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ Request::is('company-panel/*') ? route('company-panel.register') : route('register') }}" class="nav-link border border-light rounded"><i class="fa fa-user mr-2"></i>Sing Up
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ Request::is('company-panel/*') ? route('company-panel.login') : route('login') }}" class="nav-link border border-light rounded"><i class="fa fa-sign-in mr-2"></i>log in
                        </a>
                    </li>
                    @else
                    <!-- Right -->
                    <ul class="navbar-nav ml-auto nav-flex-icons">
                        <li class="nav-item">
                            @if(auth()->user()->guard == 'web')
                            <form class="form-inline" method="GET" action="{{ url('/search') }}">
                                <div class="md-form my-0">
                                    <input class="form-control mr-sm-2" name="search" type="text" placeholder="Search" aria-label="Search">
                                </div>
                            </form>
                            @else
                            <form class="form-inline" method="GET" action="{{ url('/company-panel/search') }}">
                                <div class="md-form my-0">
                                    <input class="form-control mr-sm-2" name="search" type="text" placeholder="Search" aria-label="Search">
                                </div>
                            </form>
                            @endif
                        </li>
                        <li class="nav-item avatar dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <img src="{{ Request::is('company-panel/*') || Request::is('company-panel') ? getImage('company', $auth->image) : getImage('user', $auth->image) }}" class="rounded-circle" alt="avatar image" style="width:30px; height:30px;">
                            </a>
                            @php
                                $user = route('users.profile', ['id' => Auth::id()]);
                                $company = route('company.profile', ['id' => Auth::guard('company')->id()]);
                                $profile = Request::is('company-panel/*') || Request::is('company-panel') ? $company : $user;
                            @endphp
                            <div class="dropdown-menu dropdown-menu-left dropdown-secondary" aria-labelledby="navbarDropdownMenuLink-55">
                                <a class="dropdown-item" href="{{ $profile }}">My account</a>
                                @if(Auth::user()->guard == 'web')
                                    <a class="dropdown-item" href="{{ route('users.my-jobs') }}">My Jobs</a>
                                @endif
                                @if (auth()->user()->guard == 'company')
                                    <a class="dropdown-item" href="{{ route('company.jobs') }}">Our Jobs</a>
                                @endif
                                <form id="logout-form" action="{{ Request::is('company-panel') ? route('company.logout') : route('logout') }}" method="POST" style="display: none;">@csrf</form>

                                <a onclick="event.preventDefault();document.getElementById('logout-form').submit();" title="logout" href="{{ Request::is('company-panel') ? route('company.logout') : route('logout') }}">
                                    {{ __('logout') }}
                                </a>
                            </div>
                        </li>
                    </ul>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <div class="container" style="padding: 80px;">
        @yield('content')
    </div>

    @if(Request::is('register')) @include('includes.front.footer') @endif
    @include('includes.front.scripts')
</body>
</html>