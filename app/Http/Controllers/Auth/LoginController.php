<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth, Hash;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest:web')->except(['logout', 'userLogout']);
    }

    public function showLoginForm()
    {
        return view('auth.pages.login');
    }

    public function login(Request $request)
    {
        $validation['email']    = 'required|email';
        $validation['password'] = 'required|string';
        
        $credentials['email']    = $request->email;
        $credentials['password'] = $request->password;
        $credentials['status']   = 1;

        $remember = $request->remember;
        
        $this->validate($request, $validation);

        if (Auth::guard('web')->attempt($credentials, $remember)) {

            return redirect()->intended('/home');
        }
        return back()->withInput($request->only('email', 'remember'));
    }

    public function userLogout()
    {
        Auth::guard('web')->logout();
        return redirect('/');
    }
}
