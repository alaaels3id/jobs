@extends('layouts.app')

@section('title', 'My Profile')

@section('style')
<style>
	.bg-navy  { background-color: #374258; color:#fff;  }
	.bg-white { background-color: #fff; color:#374258;  }
</style>
@stop

@section('content')
@if(Session::has('success')) <div class="alert alert-success">{{ Session::get('success') }}</div> @endif
<div class="row wow fadeIn">
	<div class="col-md-8">
		<br>
		@forelse($jobs as $job)
		<div class="card bg-navy">
			<div class="card-header">Applyed Jobs</div>
			<div class="card-body bg-white">
				<b>Job Name</b> : {{ ucwords($job->jop->name) }} <br>
				<b>Job Description</b> : {{ ucwords($job->jop->description) }} <br>
				<b>Job Salary</b> : {{ ucwords($job->jop->salary) }} L.E <br>
				<b>Since</b> : {{ $job->created_at->diffForHumans() }} <br>
			</div>
		</div>
		<br>
		@empty
		<div class="card bg-navy">
			<div class="card-header">Applyed Jobs</div>
			<div class="card-body bg-white">
				No Jobs
			</div>
		</div>
		@endforelse
	</div>
	<div class="col-md-4">
		<br>
		<div class="card bg-navy">
			<div class="card-header">Courses ({{ getUserCourses($auth->id)->count() }})</div>
			<div class="card-body bg-white">
				<ul class="list-group">
				@forelse(getUserCourses($auth->id) as $course)
					<li class="list-group-item">{{ $course->name }}</li>
				@empty
				<li>No Courses</li>
				@endforelse
				</ul>
			</div>
		</div>
		<div class="card bg-navy">
			<div class="card-header">Applyed Jobs ({{ getUserApplyedJobs($auth->id)->count() }})</div>
			<div class="card-body bg-white">
				<ul class="list-group">
				@forelse(getUserApplyedJobs($auth->id) as $job)
					<li class="list-group-item">{{ $job->jop->name }}</li>
				@empty
				<li class="list-group-item">No Jobs</li>
				@endforelse
				</ul>
			</div>
		</div>
	</div>
</div>
@stop