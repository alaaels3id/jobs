@extends('backend.layouts.app')

@section('title', ucwords(str()->plural($model[0])))

@section('content')

    <section class="content-header">
        <h1>Dashboard<small>Control panel</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>
                <a href="{{ route(str()->plural($model[0]).'.index') }}">
                    <i class="ion {{ $model[1] }}"></i>{{ ucwords(str()->plural($model[0])) }}
                </a>
            </li>
            <li class="active">edit</li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-info">
            <div class="box-header">Edit {{ ucfirst($model[0]) }} {{ $editor->name }}</div>
            {!! 
                Form::model($editor,[
                    'url' => url('/admin-panel/'.str()->plural($model[0]).'/'.$editor->id), 
                    'method' => 'PUT','class' => 'form-horizontal '.str()->plural($model[0]).' ajax edit',
                    'files' => true
                ])
            !!}
            <div class="box-body">
                @include('Backend.partials.flash')
                <div class="block block-rounded">
                    <div class="block-header bg-smooth-dark col-md-10 col-md-offset-1">
                        
                        @if($model[2] != null)
                            @include('Backend.'.str()->plural($model[0]).'.form', ['list' => $model[2]])
                        @else
                            @include('Backend.'.str()->plural($model[0]).'.form')
                        @endif

                    </div>
                </div>
            </div>
            <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary" value="Save">
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@stop