<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCourseRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'user_id'       => 'required|numeric|exists:users,id',
            'city_id'       => 'required|numeric|exists:cities,id',
            'department_id' => 'required|numeric|exists:departments,id',
            'name'          => 'required|string',
        ];

        return $rules;
    }
}
