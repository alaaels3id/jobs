<?php

namespace App\Http\Controllers\Auth\Company;

use App\Models\Company;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;

class CompanyRegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/company-panel';

    public function __construct()
    {
        $this->middleware('guest:company');
    }
    
    public function showRegistrationForm()
    {
        return view('auth.pages.companyRegister');
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($company = $this->create($request->all())));

        $this->guard()->login($company);

        return $this->registered($request, $company) ?: redirect('/company-panel');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|string|max:255',
            'email'    => 'required|string|email|max:255|unique:users',
            'address'  => 'required|string|max:255',
            'phone'    => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    protected function create(array $data)
    {
        return Company::create([
            'name'          => $data['name'],
            'address'       => $data['address'],
            'phone'         => $data['phone'],
            'email'         => $data['email'],
            'city_id'       => $data['city_id'],
            'department_id' => $data['department_id'],
            'password'      => $data['password'],
        ]);
    }

    protected function guard()
    {
        return Auth::guard();
    }

    protected function registered(Request $request, $user)
    {
        //
    }
}
