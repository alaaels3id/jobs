<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class EditJopRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {        
        $rules = [
            'name'        => 'required|string',
            'description' => 'required|string',
            'salary'      => 'required|numeric',
        ];

        return $rules;
    }
}
