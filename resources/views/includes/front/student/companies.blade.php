@extends('layouts.app')

@section('title', 'Companies')

@section('style')
<style>
	.bg-navy  { background-color: #374258; color:#fff;  }
	.bg-white { background-color: #fff; color:#374258;  }
</style>
@stop

@section('content')
<div class="row wow fadeIn">
	<div class="col-md-8">
		<br>
		@forelse($companies as $company)
		<div class="card bg-navy">
			<div class="card-header">#{{ $company->id }} - {{ ucwords($company->name) }}</div>
			<div class="card-body bg-white">
				@php
					$count = company_jop()->where('company_id',$company->id)->get()->count();
				@endphp
				<img src="{{ getImage('company', $company->image) }}" class="rounded-circle" width="60" height="60" alt=""><br><br>
				<b>Company Name</b> : {{ ucwords($company->name) }} <br>
				<b>Company Department</b> : {{ ucwords($company->department->name) }} <br>
				<b>Company City</b> : {{ ucwords($company->city->name) }} <br>
				<b>Company Email</b> : {{ ucwords($company->email) }} <br>
				<b>Company Address</b> : {!! ucwords(getDeafultValue($company->address)) !!} <br>
				<b>Company Phone</b> : {!! getDeafultValue($company->phone) !!} <br>
				<b>Company Jobs</b> : {{ $count }} <br>
				<b>Company Description</b> : {!! ucwords(getDeafultValue($company->description)) !!} <br>
				<b>Since</b> : {{ $company->created_at->diffForHumans() }} <br>
			</div>
		</div>
		<br>
		@empty
		<div class="card bg-navy">
			<div class="card-header">Applyed Jobs</div>
			<div class="card-body bg-white">
				No Jobs
			</div>
		</div>
		@endforelse
	</div>
	<div class="col-md-4">
		<br>
		<div class="card bg-navy">
			<div class="card-header">Courses ({{ getUserCourses($auth->id)->count() }})</div>
			<div class="card-body bg-white">
				<ul class="list-group">
				@forelse(getUserCourses($auth->id) as $course)
					<li class="list-group-item">{{ $course->name }}</li>
				@empty
				<li>No Courses</li>
				@endforelse
				</ul>
			</div>
		</div>
		<div class="card bg-navy">
			<div class="card-header">Applyed Jobs ({{ getUserApplyedJobs($auth->id)->count() }})</div>
			<div class="card-body bg-white">
				<ul class="list-group">
				@forelse(getUserApplyedJobs($auth->id) as $job)
					<li class="list-group-item">{{ $job->jop->name }}</li>
				@empty
				<li class="list-group-item">No Jobs</li>
				@endforelse
				</ul>
			</div>
		</div>
	</div>
</div>
@stop