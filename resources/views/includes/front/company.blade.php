@extends('layouts.app')

@section('title', 'Company Profile')

@section('style')
<style>
	.bg-navy  { background-color: #374258; color:#fff;  }
	.bg-white { background-color: #fff; color:#374258;  }
	.company-image { border-radius: 50%; padding: 5px; margin: 5px; display: inline-block;}
</style>
@stop

@section('content')
<div class="row wow fadeIn">
	<div class="col-md-8">
		@forelse(getCompanyJops($company->id) as $com)
		<div class="card bg-navy">
			<div class="card-header">{{ ucwords($com->jop->name) }}</div>
			<div class="card-body bg-white lead">
				<img src="{{ getImage('company', $company->image) }}" class="company-image" width="60" height="60" alt=""><br><br>
				<p class="lead">
					<b>{{ $company->name }}</b> is posted a new job in {{ $company->department->name }} <br>
				for job title {{ $com->jop->name }} since <b>{{ $com->created_at->diffForHumans() }}</b>
				</p>
			</div>
		</div>
		<br>
		@empty
		<div class="card bg-navy">
			<div class="card-header">{{ ucwords($company->name) }} Jops</div>
			<div class="card-body bg-white">No Jobs</div>
		</div>
		@endforelse
	</div>
	<div class="col-md-4">
		<div class="card bg-navy">
			<div class="card-header">{{ ucwords($company->name) }}</div>
			<div class="card-body bg-white">
				<ul class="list-group">
					<li class="list-group-item">
						<img src="{{ getImage('company', $company->image) }}" width="220" height="220" title="{{ $company->name }}" alt="{{ $company->name }}">
					</li>
					<li class="list-group-item"><b>Name:</b> {{ $company->name }}</li>
					<li class="list-group-item"><b>City:</b> {{ $company->city->name }}</li>
					<li class="list-group-item"><b>Department:</b> {{ $company->department->name }}</li>
					<li class="list-group-item"><b>Email:</b> <a href="mailto:{{ $company->email }}">{{ $company->email }}</a></li>
					<li class="list-group-item"><b>Description:</b> {{ $company->description }}</li>
					<li class="list-group-item"><b>Address:</b> {{ $company->address }}</li>
					<li class="list-group-item"><b>Phone:</b> {{ $company->phone }}</li>
				</ul>
			</div>
		</div>
	</div>
</div>
@stop