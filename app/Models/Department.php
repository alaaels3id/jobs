<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hash, DB, Image, Auth;

class Department extends Model
{
	use SoftDeletes;
	
    protected $guarded = ['id'];

    protected $casts = ['status' => 'boolean'];

    public function scopeStatus($query, $parm)
    {
        return $query->where('status', $parm);
    }

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public static function uploadImage($img)
    {
        $filename = 'department_' . str_random(12) . '_' . date('Y-m-d') . '.' . strtolower($img->getClientOriginalExtension());

        if (!file_exists(public_path('uploaded/departments/')))
            mkdir(public_path('uploaded/departments/'), 0777, true);

        $path = public_path('uploaded/departments/');
        
        $img = Image::make($img)->save($path . $filename);

        return $filename;
    }

    public static function createDepartment($departmentData) 
    {
        if (request()->hasFile('image')) $departmentData['image'] = Department::uploadImage($departmentData['image']);
        
        else $departmentData = array_except($departmentData, ['image']);
        
        $createdDepartment = Department::updateOrcreate(array_except($departmentData, ['_token']));

        return $createdDepartment;
    }

    public static function updateDepartment($departmentData, $currentDepartment)
    {
        if (request()->hasFile('image')) $departmentData['image'] = Department::uploadImage($departmentData['image']);
        
        else $departmentData = array_except($departmentData, ['image']);

        $updateDepartment = $currentDepartment->update($departmentData);

        return $updateDepartment;
    }

    public static function updateDepartmentStatus($departmentData, $currentDepartment)
    {
        $value = $departmentData['value'];

        // if the value comes with checked that mean we want the reverse value of it;
        return ($value == 'checked') ? $currentDepartment->update(['status' => 0]) : $currentDepartment->update(['status' => 1]);
    }

    public static function getInSelectForm($with_main = null , $with_null = null, $exceptedIds = [] )
    {
        $departments   = [];

        $with_null == null ? $departments = $departments : $departments = $departments + [''  => ''];
        $with_main == null ? $departments = $departments : $departments = $departments + ['0' => 'Main'];

        $departmentsDB = Department::whereNotIn('id',$exceptedIds)->get();

        foreach ($departmentsDB as $department) { $departments[$department->id] = ucwords($department->name); }

      	return $departments;
    }
}
