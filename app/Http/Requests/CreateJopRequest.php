<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateJopRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'name'     => 'required|string',
            'description'  => 'required|string',
            'salary'    => 'required|numeric',
        ];

        return $rules;
    }
}
