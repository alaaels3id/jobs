<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('admins')->insert([
            'city_id'  		 => 1,
            'status'   		 => 1,
            'is_super_Admin' => 1,
            'first_name' 	 => 'alaa',
            'last_name'      => 'elsaid',
            'address' 		 => 'Cairo, Egypt',
            'phone'          => '01011468087',
            'email'    		 => 'admin@admin.com',
            'password'       => \Hash::make('123456'),
            'created_at'     => date('Y-m-d H:i:s'),
            'updated_at'     => date('Y-m-d H:i:s'),
        ]);
    }
}
