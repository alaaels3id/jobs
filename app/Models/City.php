<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hash, DB, Image, Auth;

class City extends Model
{
	use SoftDeletes;
	
    protected $guarded = ['id'];

    protected $casts = ['status' => 'boolean'];

    public function scopeStatus($query, $parm)
    {
        return $query->where('status', $parm);
    }

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public static function uploadImage($img)
    {
        $filename = 'city_' . str_random(12) . '_' . date('Y-m-d') . '.' . strtolower($img->getClientOriginalExtension());

        if (!file_exists(public_path('uploaded/cities/')))
            mkdir(public_path('uploaded/cities/'), 0777, true);

        $path = public_path('uploaded/cities/');
        
        $img = Image::make($img)->save($path . $filename);

        return $filename;
    }

    public static function createCity($cityData) 
    {
        if (request()->hasFile('image')) $cityData['image'] = City::uploadImage($cityData['image']);
        
        else $cityData = array_except($cityData, ['image']);
        
        $createdCity = City::updateOrcreate(array_except($cityData, ['_token']));

        return $createdCity;
    }

    public static function updateCity($cityData, $currentCity)
    {
        if (request()->hasFile('image')) $cityData['image'] = City::uploadImage($cityData['image']);
        
        else $cityData = array_except($cityData, ['image']);

        $updateCity = $currentCity->update($cityData);

        return $updateCity;
    }

    public static function updateCityStatus($cityData, $currentCity)
    {
        $value = $cityData['value'];

        // if the value comes with checked that mean we want the reverse value of it;
        return ($value == 'checked') ? $currentCity->update(['status' => 0]) : $currentCity->update(['status' => 1]);
    }

    public static function getInSelectForm($with_main = null , $with_null = null, $exceptedIds = [] )
    {
        $cities   = [];

        $with_null == null ? $cities = $cities : $cities = $cities + [''  => ''];
        $with_main == null ? $cities = $cities : $cities = $cities + ['0' => 'Main'];

        $citiesDB = City::whereNotIn('id',$exceptedIds)->get();

        foreach ($citiesDB as $city) 
        {
            $cities[$city->id] = ucwords($city->name);
        }

      return $cities;
    }
}
