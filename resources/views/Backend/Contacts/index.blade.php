@extends('backend.layouts.app')

@section('title', 'Conatcts')

@section('styles')
	<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@stop

@section('content')
    
    <section class="content-header">
        <h1>Dashboard<small>Control panel</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Conatcts</li>
        </ol>
    </section>

    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box">
    				<div class="box-header">
    					<h3 class="box-title">Data Table With Full Features</h3>
    				</div>
    				<!-- /.box-header -->
    				<div class="box-body">
    					<table id="contacts" class="table table-bordered table-striped text-center">
    						<thead>
    							<tr>
    								<th>#</th>
				                  	<th>Name</th>
				                  	<th>Email</th>
				                  	<th>Subject</th>
				                </tr>
	                		</thead>
	                		<tbody data-count={{ $contacts->count() }}>
			                	@forelse($contacts as $key => $contact)
				                <tr id="contact-row-{{ $contact->id }}">
				                	<td>{{ (int)$key + 1 }}</td>
				                	<td>{{ ucwords($contact->name) }}</td>
				                	<td>{{ ucwords($contact->email) }}</td>
				                	<td>{{ ucwords($contact->subject) }}</td>
				                </tr>
			                	@endforeach
	                		</tbody>
	                		<tfoot>
	                			<tr>
    								<th>#</th>
				                  	<th>Name</th>
				                  	<th>Email</th>
				                  	<th>Subject</th>
	                			</tr>
	                		</tfoot>
	              		</table>
	            	</div>
	            	<!-- /.box-body -->
	          	</div>
	          	<!-- /.box -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@stop

@section('scripts')
	<script src="{{ asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script>

		$(function () {
			$('#contacts').DataTable({
	      		'paging'      : true,
	      		'lengthChange': true,
	      		'searching'   : true,
			    'ordering'    : true,
			    'info'        : true,
			    'autoWidth'   : true
	    	})
	  	});

	</script>
@stop