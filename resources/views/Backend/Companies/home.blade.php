@extends('layouts.app')

@section('title', 'Home')

@section('style')
<style>
    .bg-navy  { background-color: #374258; color:#fff;  }
    .bg-white { background-color: #fff; color:#374258;  }
</style>
@stop

@section('content')
<main class="mt-5 pt-5">
	<div class="container">
		@if(Session::has('success'))<div class="alert alert-success">{{ Session::get('success') }}</div>@endif
		<!--Section: Jumbotron-->
		<section class="card wow fadeIn" style="background-image: url('{{ asset('front/img/2.jpg') }}');">
			<!-- Content -->
			<div class="card-body text-body text-center py-5 px-5 my-5">
				<h1 class="mb-4"><strong>Learn and Apply in your future </strong></h1>
				<p><strong>Best & free guide of responsive Jobs</strong></p>
				<p class="mb-4">
					<strong class="justify-content-start">The most comprehensive tutorial for vacancy job. Loved by over 500 000 users...</strong>
    			</p>
    			<button type="button" class="btn btn-blue-grey btn-lg" data-toggle="modal" data-target="#myModal">Add Job</button>
    		</div>
    		<!-- Content -->
    	</section>
    	<!--Section: Jumbotron-->
   		<section class="row policy">
   			<div id="myModal" class="modal fade" role="dialog">
   				<div class="modal-dialog">
      				<div class="modal-content">
      					<div class="modal-header">
      						<h4 class="modal-title text-primary">Add new job</h4>
      						<button type="button" class="close" data-dismiss="modal">&times;</button>
      					</div>
			   			<form action="{{ route('company.jops',['id'=>$auth->id]) }}" method="POST" class="ajax edit">
			   				@csrf
	      					<div class="modal-body">
	      						<div class="form-group">
	      							<label for="name">Name</label>
	      							<input type="text" name="name" value="" class="form-date form-control">
	      						</div>
	      						<div class="form-group">
	      							<label for="salary">Salary</label>
	      							<input type="number" name="salary" value="" class="form-date form-control">
	      						</div>
								<div class="form-group">
	      							<label for="description">Description</label>
	      							<textarea name="description" value="" class="form-date form-control"></textarea>
	      						</div>
	          				</div>
	          				<div class="modal-footer">
	          					<input type="submit" name="submit" class="btn btn-info btn-sm" value="Add Job" />
	          				</div>
				          	{!! Form::close() !!}
				        </form>
          			</div>
          		</div>
          	</div>
        </section>
        <!--end modal--> 
        <hr class="my-5">
        <!--Section: Magazine v.1-->
        <section class="wow fadeIn">
        	<!--Section heading-->
        	<h2 class="h1 text-center my-5 font-weight-bold">Available Jobs</h2>
        	<!--Grid row-->
        	<div class="row text-left">
        		<!--Grid column-->
        		<div class="col-md-12" style="background-color: #fff; color: #000;">
        			<br>
        			@forelse(getCompanyJops($auth->id) as $job)
        			<!--Small news-->
        			<div class="row">
        				<div class="col-md-3">
        					<!--Image-->
        					<div class="view overlay rounded z-depth-1">
        						<img src="{{ asset('front/img/photo11.jpg') }}" class="img-fluid" alt="Minor sample post image">
        						<a><div class="mask rgba-white-slight"></div></a>
        					</div>
					    </div>
					    <!--Excerpt-->
					    <div class="col-md-9">
					    	<p class="dark-black-text"><strong>{{ $job->created_at->format('d/m/Y') }}</strong></p>
					    	<a><b>{{ ucwords($job->jop->name) }}</b><i class="fa fa-angle-right float-right"></i></a>
					    </div>
					</div>
					<!--/Small news-->
					<hr>
          			@empty
          			<div class="alert alert-info">No JOBS</div>
          			@endforelse
				</div>
   	 		</div>
   	 	</section>
   	 	<!--/Section: Magazine v.1-->
   	</div>
</main>
@stop