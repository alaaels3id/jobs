@extends('backend.layouts.app')

@section('title', 'Student Profile')

@section('content')

<section class="content-header">
    <h1 class="pull-left hidden-xs">Student Profile</h1>
    <ol class="breadcrumb">
        <li class="h4"><a href="{{ route('student-panel') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active h4">Student Profile</li>
    </ol>
</section>

<br><br>

<section class="content">
    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">About Me</h3>
                </div>

                <div class="box-body">
                    <img class="profile-user-img img-responsive img-circle" src="{{ getImage('user', null) }}" alt="Student profile picture">
                    <h3 class="profile-username text-center">{{ $student->name }}</h3>
                    <p class="text-muted lead text-center">{{ ucwords(getUserGuard()) }}</p>

                    <hr>

                    <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                    <p class="text-muted lead">{{ $student->email }}</p>

                    <hr>
                    <strong><i class="fa fa-clock-o margin-r-5"></i> Join Since</strong>

                    <p class="text-muted lead">{{ $student->created_at->diffForHumans() }}</p>

                    <hr>
                    <strong><i class="fa fa-phone margin-r-5"></i> Phone</strong>
                    <p class="text-muted lead">+2{{ $student->phone }}</p>

                    <hr>

                    <strong><i class="fa fa-calendar margin-r-5"></i>Current Year</strong>
                    <p class="text-muted lead">Year num : {{ $student->current_year }}</p>
    
                    <hr>

                    <strong><i class="fa fa-id-card-o margin-r-5"></i>Address</strong>
                    <p class="text-muted lead">{{ $student->address }}</p>

                    <hr>

                    <strong><i class="fa fa-id-card-o margin-r-5"></i>Paid</strong>
                    <p class="text-muted lead">{!! $student->paid == 0 ? '<span class="label label-danger">Not Paid</span>' : '<span class="label label-success">Paid</span>' !!}</p>
                </div>
            </div>
        </div>
        
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Settings</h3>
                        </div>

                        {!! 
                            Form::model($auth,['route'=>['student.updateprofile',$auth->id],'method'=>'POST','files'=>true,'class'=>'ajax edit']) 
                        !!}
                        <div class="box-body">

                            <div class="form-group">
                                @include('includes.inputs', ['errors'=>$errors,'type'=>'text','name'=>'name','style'=>'form-control form-data'])
                                @include('includes.inputs', ['errors'=>$errors,'type'=>'email','name'=>'email','style'=>'form-control form-data'])
                            </div>

                            <div class="form-group">
                                @include('includes.inputs', ['errors'=>$errors,'type'=>'text','name'=>'phone','style'=>'form-control form-data'])
                                @include('includes.inputs', ['errors'=>$errors,'type'=>'text','name'=>'address','style'=>'form-control form-data'])
                            </div>
                            <div class="form-group">
                                @include('includes.inputs',['errors'=>$errors,'type'=>'password','name'=>'password','style'=>'form-control form-data'])
                                @include('includes.inputs',[
                                    'errors'=>$errors,'type'=>'password_confirmation',
                                    'name'=>'password_confirmation','style'=>'form-control form-data'
                                ])
                            </div>
                        </div>

                        <div class="box-footer">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>

                    @if(auth()->user()->current_year == 4)
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Project</h3>
                            <div class="box-body">
                                {!! Form::open(['route'=>['student.updateProject',$auth->id], 'method' => 'POST', 'class' => 'ajax edit']) !!}
                                <div class="form-group">
                                    @include('includes.inputs', [
                                        'errors' => $errors, 'type' => 'select', 
                                        'name' => 'project_id', 'style' => 'form-control form-data',
                                        'list' => getAvailableProject()
                                    ])
                                </div>
                            </div>
                            <div class="box-footer">
                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@stop