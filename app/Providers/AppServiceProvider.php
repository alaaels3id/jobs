<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema, View;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    public function register()
    {
        View::composer('*', function($view){
          return $view->with('auth', auth()->user());
        });
    }
}
