<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyJop extends Model
{
    protected $guarded = ['id'];

    public function jop()
    {
        return $this->belongsTo('App\Models\Jop', 'jop_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public static function createCompanyJop($companyJopData)
    {
        $createdJop = Jop::updateOrcreate([
            'name' => $companyJopData['name'],
            'salary' => $companyJopData['salary'],
            'description' => $companyJopData['description']
        ]);
        $createdCompanyJop = CompanyJop::updateOrcreate([
            'company_id'=>$companyJopData['company_id'],
            'jop_id'=>$createdJop->id,
        ]);

        return $createdCompanyJop;
    }

    public static function updateCompanyJop($companyJopData, $currentCompanyJop)
    {
        $updateCompanyJop = $currentCourse->update($companyJopData);

        return $updateCompanyJop;
    }
}
