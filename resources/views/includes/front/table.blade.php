<div class="box box-{{ $color }}">
    <div class="box-header with-border">
        <h3 class="box-title">Latest {{ plural($model) }}</h3>

        <div class="box-tools pull-right">
            <span class="label label-{{ $color }}">8 New {{ plural($model) }}</span>
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>

    <div class="box-body no-padding">
        <ul class="users-list clearfix">
            @forelse($data as $value)
            <li>
                <img style="width: 120px;height: 120px;" src="{{ getImage($model, $value->image) }}" alt="User Image">
                <a class="users-list-name" href="javascript:void();">{{ $model != 'company' ? $value->fullname : $value->name }}</a>
                <span class="users-list-date">{{ $value->created_at->diffForHumans() }}</span>
            </li>
            @empty
                <div style="margin: 13px;" class="alert alert-{{ $color }} {{ $color != 'info' ? 'bg-blue' : '' }} text-center">No Data</div>
            @endforelse
        </ul>
    </div>

    <div class="box-footer text-center">
        <a href="{{ route(plural($model, false).'.index') }}" class="uppercase">View All {{ plural($model) }}</a>
    </div>
</div>