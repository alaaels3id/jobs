<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| $2y$10$fcCiT9H3uH6lFs.UmFmFO.IQ3IO.cfCIzlLt5z8/0kRJ4tSBI1kC2 => 123456
*/

Auth::routes();
Route::get('/','HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::view('/about', 'includes.front.about')->name('about');
Route::view('/contact', 'includes.front.contact')->name('contact');
Route::post('/logout', 'Auth\LoginController@userLogout')->name('logout');

// Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\ResetPasswordController@showResetForm')->name('password.update');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/email','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');

# Admin Login
Route::group(['prefix' => 'admin-panel', 'namespace' => 'Auth\Admin'], function() {
	Route::get('/login', 'AdminLoginController@showAdminLoginForm')->name('admin-panel.login');
	Route::post('/login', 'AdminLoginController@adminLogin')->name('admin.submit.login');
	Route::post('/logout', 'AdminLoginController@adminLogout')->name('admin.logout');
});

# Company Login
Route::group(['prefix' => 'company-panel', 'namespace' => 'Auth\Company'], function() {
	Route::get('/login', 'CompanyLoginController@showCompanyLoginForm')->name('company-panel.login');
	Route::get('/register', 'CompanyRegisterController@showRegistrationForm')->name('company-panel.register');
	Route::post('/register', 'CompanyRegisterController@register')->name('company.register');
	Route::post('/login', 'CompanyLoginController@companyLogin')->name('company.submit.login');
	Route::post('/logout', 'CompanyLoginController@companyLogout')->name('company.logout');
});

# Admin routes
Route::group(['prefix' => 'admin-panel', 'middleware' => 'auth:admin', 'namespace' => 'Backend'], function() 
{
	Route::get('/', 'AdminController@home')->name('admin-panel');

	// admin -> jops Routes
	Route::resource('jops', 'JopController', ['except' => ['show']]);
	Route::post('ajax-change-jop-status', 'JopController@ChangeJopStatus');
	Route::post('/jops/ajax-delete-jop', 'JopController@DeleteJop');
	Route::post('/jops/all/ajax-restore-jop', 'JopController@RestoreJop');
	Route::post('/jops/all/ajax-remove-jop', 'JopController@RemoveJop');
	Route::get('/jops/all/trashed', 'JopController@Trashed')->name('jops.trashed');

	// admin -> departments Routes
	Route::resource('departments', 'DepartmentController', ['except' => ['show']]);
	Route::post('ajax-change-department-status', 'DepartmentController@ChangeDepartmentStatus');
	Route::post('/departments/ajax-delete-department', 'DepartmentController@DeleteDepartment');
	Route::post('/departments/all/ajax-restore-department', 'DepartmentController@RestoreDepartment');
	Route::post('/departments/all/ajax-remove-department', 'DepartmentController@RemoveDepartment');
	Route::get('/departments/all/trashed', 'DepartmentController@Trashed')->name('departments.trashed');

	// admin -> companies Routes
	Route::resource('companies', 'CompanyController', ['except' => ['show']]);
	Route::post('ajax-change-company-status', 'CompanyController@ChangeCompanyStatus');
	Route::post('/companies/ajax-delete-company', 'CompanyController@DeleteCompany');
	Route::post('/companies/all/ajax-restore-company', 'CompanyController@RestoreCompany');
	Route::post('/companies/all/ajax-remove-company', 'CompanyController@RemoveCompany');
	Route::get('/companies/all/trashed', 'CompanyController@Trashed')->name('companies.trashed');

	// admin -> cities Routes
	Route::resource('cities', 'CityController', ['except' => ['show']]);
	Route::post('ajax-change-city-status', 'CityController@ChangeCityStatus');
	Route::post('/cities/ajax-delete-city', 'CityController@DeleteCity');
	Route::post('/cities/all/ajax-restore-city', 'CityController@RestoreCity');
	Route::post('/cities/all/ajax-remove-city', 'CityController@RemoveCity');
	Route::get('/cities/all/trashed', 'CityController@Trashed')->name('cities.trashed');

	// admin -> courses Routes
	Route::resource('courses', 'CourseController', ['except' => ['show']]);
	Route::post('ajax-change-course-status', 'CourseController@ChangeCourseStatus');
	Route::post('/courses/ajax-delete-course', 'CourseController@DeleteCourse');
	Route::post('/courses/all/ajax-restore-course', 'CourseController@RestoreCourse');
	Route::post('/courses/all/ajax-remove-course', 'CourseController@RemoveCourse');
	Route::get('/courses/all/trashed', 'CourseController@Trashed')->name('courses.trashed');

	// admin -> users Routes
	Route::resource('users', 'UserController', ['except' => ['show']]);
	Route::post('ajax-change-user-status', 'UserController@ChangeUserStatus');
	Route::post('/users/ajax-delete-user', 'UserController@DeleteUser');
	Route::post('/users/all/ajax-restore-user', 'UserController@RestoreUser');
	Route::post('/users/all/ajax-remove-user', 'UserController@RemoveUser');
	Route::get('/users/all/trashed', 'UserController@Trashed')->name('users.trashed');

	// admin -> Admins Routes
	Route::resource('admins', 'AdminController');
	Route::get('admin/{id}/profile', 'AdminController@adminProfile')->name('admins.profile');
	Route::post('/admin/{id}/profile', 'AdminController@AdminUpdateProfile')->name('admins.updateprofile');
	Route::post('ajax-change-admin-status', 'AdminController@ChangeAdminStatus');
	Route::post('ajax-delete-admin', 'AdminController@DeleteAdmin');
	Route::post('/admins/all/ajax-restore-admin', 'AdminController@RestoreAdmin');
	Route::post('/admins/all/ajax-remove-admin', 'AdminController@RemoveAdmin');
	Route::get('/admins/all/trashed', 'AdminController@Trashed')->name('admins.trashed');

	// admin -> Settings Routes
	Route::resource('settings', 'SettingController');
	Route::post('ajax-change-setting-status', 'SettingController@ChangeSettingStatus');
	Route::post('ajax-delete-setting', 'SettingController@DeleteSetting');
	Route::post('/settings/all/ajax-restore-setting', 'SettingController@RestoreSetting');
	Route::post('/settings/all/ajax-remove-setting', 'SettingController@RemoveSetting');
	Route::get('/settings/all/trashed', 'SettingController@Trashed')->name('settings.trashed');

	// admin -> contacts Routes
	Route::resource('contacts', 'ContactController', ['except' => ['show']]);
});

# Admin routes
Route::group(['prefix' => 'company-panel', 'middleware' => 'auth:company', 'namespace' => 'Backend'], function() 
{
	Route::get('/', 'CompanyController@home')->name('company-panel');
	Route::post('/company/{id}/jobs', 'CompanyController@AddNewJob')->name('company.jops');
	Route::get('/company/{id}/appliers', 'CompanyController@GetJobApplyers')->name('company.appliers');
	Route::get('/company/{id}/profile', 'CompanyController@companyProfile')->name('company.profile');
	Route::post('/company/{id}/profile', 'CompanyController@CompanyUpdateProfile')->name('company.updateprofile');
	Route::get('/jobs', 'CompanyController@Jobs')->name('company.jobs');
	Route::get('/company/user/{id}/profile', 'CompanyController@UserProfile')->name('company.user-profile');
	Route::get('/search', 'CompanyController@Search')->name('company.search');
});

# Student Routes
Route::group(['namespace' => 'Backend', 'middleware' => 'web'], function() {
	Route::get('/user/{id}/profile', 'UserController@UserProfile')->name('users.profile');
	Route::post('/user/{id}/profile', 'UserController@UserUpdateProfile')->name('users.updateprofile');
	Route::get('/company/{id}/profile', 'UserController@CompanyProfile')->name('users.company');
	Route::post('/user/{user_id}/jop/{jop_id}', 'UserController@SetUserJob');
	Route::post('/user/{id}/profile/project', 'UserController@SetStudentProject')->name('users.updateProject');
	Route::get('/user/{id}/courses', 'UserController@courses')->name('users.courses');
	Route::post('/contact', 'UserController@SendMessage')->name('users.contact');
	Route::get('/companies', 'UserController@Companies')->name('users.companies');
	Route::get('/jobs', 'UserController@jobs')->name('users.jops');
	Route::get('/my-jobs', 'UserController@MyJobs')->name('users.my-jobs');
	Route::post('/user/{user_id}/courses', 'UserController@AddNewCourse')->name('users.add-course');
	Route::get('/search', 'UserController@Search')->name('users.search');
});
