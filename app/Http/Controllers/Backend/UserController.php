<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\EditUserRequest;
use App\Models\Department;
use App\Models\User;
use App\Models\City;
use Auth, DB, Image, File, Validator, Response, Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('Backend.users.index', compact('users'));
    }

    public function home()
    {
        return view('Backend.users.home');
    }

    public function Trashed()
    {
        $users = User::onlyTrashed()->get();
        return view('Backend.users.trashed', compact('users'));
    }

    public function UserProfile($id)
    {
        $user = User::find($id);
        return view('includes.front.student.profile', compact('user'));
    }

    public function Search(Request $request)
    {
        $request->validate(['search' => 'string']);
        $search = $request->search;
        $results = jop()->where('name', 'LIKE', '%'. $search .'%')->where('status', 1)->get();
        return view('includes.front.search', compact('results'));
    }

    public function jobs()
    {
        $jops = getAvailableCompaniesJobs(auth()->id());
        return view('includes.front.jops', compact('jops'));
    }

    public function AddNewCourse(Request $request, $user_id)
    {
        $data = array_except($request->all(), ['_token']);
        $createdCourse = course()->updateOrcreate($data);
        if($createdCourse) return back()->with('success', 'Course added Successfully');
        return back()->with('danger', 'Can not add the course !!');
    }

    public function Companies()
    {
        $companies = company()->where('status',1)->get();
        return view('includes.front.student.companies',compact('companies'));
    }

    public function MyJobs()
    {
        $jobs = getUserApplyedJobs(auth()->id());
        return view('includes.front.student.jobs', compact('jobs'));
    }

    public function SetUserJob($user_id, $jop_id)
    {
        if(jop_user()->createUserJop(['user_id' => $user_id, 'jop_id' => $jop_id]))
        {
            return back()->with('success', 'Done');
        }
        return back()->with('danger', 'Error');
    }

    public function courses($id)
    {
        $courses = getUserCourses($id);
        return view('includes.front.courses', compact('courses'));
    }

    public function SendMessage(Request $request)
    {
        $contactData = array_except($request->all(),['_token']);
        if(contact()->updateOrcreate($contactData)) return back()->with('success', 'Send Message Successfully');
        return back()->withInputs();
    }

    public function CompanyProfile($id)
    {
        $company = company()->find($id);
        return view('includes.front.company', compact('company'));
    }

    public function UserUpdateProfile(Request $request, $id)
    {
        $user = User::find($id);
        $updatedProfile = $user->updateUser(array_except($request->all(), ['_token', 'submit']), $user);
        if($updatedProfile) return back()->with('success', 'Data Updated Successfully !');
        return back()->withInputs();
    }

    public function create()
    {
        $lists['cities'] = City::getInSelectForm();
        $lists['departments'] = Department::getInSelectForm();
        return view('Backend.users.create', compact('lists'));
    }

    public function store(CreateUserRequest $request)
    {
        $userData = $request->all();

        if(User::createUser($userData))
        {
            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Added Successfully')]);
            } else {
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Added Successfully'));
            }
        }
        else
        {
            if(request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Internal Server Error 500')]);
            } else {
                request()->session()->flash('status','danger');
                request()->session()->flash('message',__('Internal Server Error 500'));
            }

            return back()->withInputs();
        }
        return redirect()->route('users.index');
    }

    public function ChangeUserStatus(Request $request)
    {
        // Request ajax from index;
        $userData = $request->all();
        $currentUser = User::find($request->id);
        
        if (!$updateUserStatus = User::updateUserStatus($userData, $currentUser))
            return response()->json(['requestStatus' => false, 'message' => trans('Server Internal Error 500')]);

        return response()->json(['requestStatus' => true, 'message' => trans('Data Updated Successfully')]);
    }

    public function edit($id)
    {
        $user = User::find($id);
        $lists['cities'] = City::getInSelectForm();
        $lists['departments'] = Department::getInSelectForm();
        return view('Backend.users.edit', compact('user', 'lists'));
    }

    public function update(EditUserRequest $request, $id)
    {
        $userData = $request->all();
        $currentUser = User::find($id);
        
        if (User::updateUser($userData, $currentUser) ) {

            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Updated Successfully')]);
            }else{
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Updated Successfully'));
            }
        }else{
            if (request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Server Internal Error 500')]);
            }else{
                 request()->session()->flash('status','danger');
                 request()->session()->flash('message',__('Server Internal Error 500'));
            }
        }

        return redirect()->route('users.edit', ['id' => $currentUser->id]);
    }

    public function DeleteUser(Request $request)
    {
        $user = User::find($request->id);

        if(!$user) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, User is not exists !!']);

        try
        {            
            $user->delete();
            
            return response()->json(['deleteStatus' => true, 'message' => 'User Deleted Successfully']);
        }
        catch (Exception $e){ return response()->json(['deleteStatus' => false,'error' => 'Server Internal Error 500']); }

        return redirect()->route('users.index');
    }

    public function RestoreUser(Request $request)
    {
        $restore = DB::table('users')->where('id', $request->id)->where('deleted_at', '!=', null)->update(['deleted_at' => null]);
        
        $response['requestStatus']  = ($restore) ? true : false;
        $response['message']        = ($restore) ? 'User Restored Successfully' : 'Server Internal Error 500';

        return response()->json($response);
    }

    public function RemoveUser(Request $request)
    {
        $remove = DB::table('users')->where('id', $request->id)->where('deleted_at', '!=', null);

        if (isset($remove->image) && $remove->image != null) File::delete('uploaded/users/'.$remove->image);

        $remove->delete();
        
        $response['requestStatus']  = ($remove) ? true : false;

        return response()->json($response);
    }
}
