<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash, DB, Image, Auth;

class Company extends Authenticatable
{
    use Notifiable, SoftDeletes;
    
    protected $guarded = ['id', 'password_confirmation'];

    protected $casts = ['status' => 'boolean'];

    public $guard = 'company';

    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function scopeStatus($query, $parm)
    {
        return $query->where('status', $parm);
    }

    public function setStatusAttribute($value) //mutator for active column.
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public static function uploadImage($img)
    {
        $filename = 'company_' . str_random(12) . '_' . date('Y-m-d') . '.' . strtolower($img->getClientOriginalExtension());

        if (!file_exists(public_path('uploaded/companies/')))
            mkdir(public_path('uploaded/companies/'), 0777, true);

        $path = public_path('uploaded/companies/');
        
        $img = Image::make($img)->save($path . $filename);

        return $filename;
    }

    public static function createCompany($companyData) 
    {
        if (request()->hasFile('image')) $companyData['image'] = Company::uploadImage($companyData['image']);
        
        else $companyData = array_except($companyData, ['image']);

        $createdCompany = Company::updateOrcreate(array_except($companyData, ['_token']));

        return $createdCompany;     
    }

    public static function updateCompany($companyData, $currentCompany)
    {
        if (request()->hasFile('image')) $companyData['image'] = Company::uploadImage($companyData['image']);
        
        else $companyData = array_except($companyData, ['image']); 

        $updateCompany = $currentCompany->update($companyData);

        return $updateCompany;
    }

    public static function updateCompanyStatus($companyData, $currentCompany)
    {
        $value = $companyData['value'];

        // if the value comes with checked that mean we want the reverse value of it;
        return ($value == 'checked') ? $currentCompany->update(['status' => 0]) : $currentCompany->update(['status' => 1]);
    }

    public static function getInSelectForm($with_main = null , $with_null = null, $exceptedIds = [] )
    {
        $companies   = [];

        $with_null == null ? $companies = $companies : $companies = $companies + [''  => ''];
        $with_main == null ? $companies = $companies : $companies = $companies + ['0' => 'Main'];

        $companiesDB = Company::whereNotIn('id',$exceptedIds)->get();

        foreach ($companiesDB as $company) { $companies[$company->id] = ucwords($company->name); }

      	return $companies;
    }
}
