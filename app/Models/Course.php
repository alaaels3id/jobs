<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hash, DB, Image, Auth;

class Course extends Model
{
	use SoftDeletes;
	
    protected $guarded = ['id'];

    protected $casts = ['status' => 'boolean'];

    public function scopeStatus($query, $parm)
    {
        return $query->where('status', $parm);
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public static function uploadImage($img)
    {
        $filename = 'course_' . str_random(12) . '_' . date('Y-m-d') . '.' . strtolower($img->getClientOriginalExtension());

        if (!file_exists(public_path('uploaded/courses/')))
            mkdir(public_path('uploaded/courses/'), 0777, true);

        $path = public_path('uploaded/courses/');
        
        $img = Image::make($img)->save($path . $filename);

        return $filename;
    }

    public static function createCourse($courseData) 
    {
        if (request()->hasFile('image')) $courseData['image'] = Course::uploadImage($courseData['image']);
        
        else $courseData = array_except($courseData, ['image']);
        
        $createdCity = Course::updateOrcreate(array_except($courseData, ['_token']));

        return $createdCity;
    }

    public static function updateCourse($courseData, $currentCourse)
    {
        if (request()->hasFile('image')) $courseData['image'] = Course::uploadImage($courseData['image']);
        
        else $courseData = array_except($courseData, ['image']);

        $updateCourse = $currentCourse->update($courseData);

        return $updateCourse;
    }

    public static function updateCourseStatus($courseData, $currentCourse)
    {
        $value = $courseData['value'];

        // if the value comes with checked that mean we want the reverse value of it;
        return ($value == 'checked') ? $currentCourse->update(['status' => 0]) : $currentCourse->update(['status' => 1]);
    }

    public static function getInSelectForm($with_main = null , $with_null = null, $exceptedIds = [] )
    {
        $courses   = [];

        $with_null == null ? $courses = $courses : $courses = $courses + [''  => ''];
        $with_main == null ? $courses = $courses : $courses = $courses + ['0' => 'Main'];

        $coursesDB = Course::whereNotIn('id',$exceptedIds)->get();

        foreach ($coursesDB as $course) { $courses[$course->id] = ucwords($course->name); }

      	return $courses;
    }
}
