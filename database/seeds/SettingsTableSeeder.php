<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('settings')->insert([
            'admin_id'   => 1,
            'key'        => 'site_name',
            'value'      => 'CTJ website egypt',
            'status'     => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('settings')->insert([
            'admin_id'   => 1,
            'key'        => 'site_email',
            'value'      => 'contact@ctj-egypt.net',
            'status'     => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('settings')->insert([
            'admin_id'   => 1,
            'key'        => 'site_mobile',
            'value'      => '0123456789',
            'status'     => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
