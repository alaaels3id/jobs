<header class="main-header">
	
	<a href="{{ url('/') }}" class="logo">
		<span class="logo-mini"><b>A</b>DP</span>
		<span class="logo-lg"><b>Admin </b>Panal</span>
	</a>
	
	<nav class="navbar navbar-static-top">
		
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				
				<li class="dropdown messages-menu">
					
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-envelope-o"></i>
						<span class="label label-success">10</span>
					</a>
					
					<ul class="dropdown-menu">
						<li class="header">
							You have 10 unreaded
						</li>
						<li>
							<ul class="menu">
								<li>
									<a href="{{ url('admin-panal/contactus/edit') }}">
										<div class="pull-left">
											<img src="#" class="img-circle" alt="User Image">
										</div>
										<h4>{{ ucfirst('alaa') }}<small><i class="fa fa-clock-o"></i> alaa </small></h4>
										<p><span><i class="label label-info"></i> Subject : alaa</span><br>alaa</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="footer"><a href="{{ url('admin-panal/contactus') }}">See All Messages</a></li>
					</ul>
				</li>
				
				<li class="dropdown notifications-menu">
					
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-bell-o"></i>
						<span class="label label-warning">20</span>
					</a>
 
					<ul class="dropdown-menu">
						<li class="header">
							You have 20 notifications
						</li>
						<li>
							<ul class="menu">
								<li>
									<div class="pull-left">
										<i class="fa fa-users text-red"></i>
										<a class="btn btn-default text-center" href="{{ url('/admin-panal/buildings/edit') }}">name</a>
									</div>
									<a class="pull-right btn text-center" href="{{ url('/admin-panal/ChangeStatus/Active') }}">Set Active</a>
									<div class="clearfix"></div>
								</li>
							</ul>
						</li>
						<li class="footer"><a href="#">View all</a></li>
					</ul>
				</li>
				
				<li class="dropdown user user-menu">
					
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="#"  class="user-image" alt="IMG">
						<span class="hidden-xs">Alaa</span>
					</a>
					
					<ul class="dropdown-menu">
						
						<li class="user-header">
							<img src="#" class="img-circle" alt="User Image">
							<p>Alaa - Web Developer<small>Member since Nov. 20-04-2019</small></p>
						</li>
						
						<li class="user-body">
							<div class="row">
								<div class="col-xs-4 text-center"><a href="#">Followers</a></div>
								<div class="col-xs-4 text-center"><a href="#">Sales</a></div>
								<div class="col-xs-4 text-center"><a href="#">Friends</a></div>
							</div>
						</li>
						
						<li class="user-footer">
							
							<div class="pull-left">
								<a href="{{ url('/admin-panal/profile/alaa') }}" class="btn btn-default btn-flat">Profile</a>
							</div>
							
							<div class="pull-right">
								<a class="btn btn-default btn-flat" href="{{ route('logout') }}"
	                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
	                               {{ __('Sign out') }}
	                            </a>
	                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
                            	</form>
	                        </div>
						</li>

					</ul>
				</li>
				
				<li><a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a></li>
			</ul>
		</div>
	</nav>
</header>