<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDepartmentRequest;
use App\Http\Requests\EditDepartmentRequest;
use App\Models\Department;
use Auth, DB, Image, File, Validator, Response, Hash;

class DepartmentController extends Controller
{
    public function index()
    {
        $departments = Department::all();
        return view('Backend.departments.index', compact('departments'));
    }

    public function home()
    {
        return view('Backend.departments.home');
    }

    public function Trashed()
    {
        $departments = Department::onlyTrashed()->get();
        return view('Backend.departments.trashed', compact('departments'));
    }

    public function create()
    {
        return view('Backend.departments.create');
    }

    public function store(CreateDepartmentRequest $request)
    {
        $departmentData = $request->all();

        if(Department::createDepartment($departmentData))
        {
            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Added Successfully')]);
            } else {
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Added Successfully'));
            }
        }
        else
        {
            if(request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Internal Server Error 500')]);
            } else {
                request()->session()->flash('status','danger');
                request()->session()->flash('message',__('Internal Server Error 500'));
            }

            return back()->withInputs();
        }
        return redirect()->route('departments.index');
    }

    public function ChangeDepartmentStatus(Request $request)
    {
        // Request ajax from index;
        $departmentData = $request->all();
        $currentDepartment = Department::find($request->id);
        
        if (!$updatedepartmentstatus = Department::updateDepartmentStatus($departmentData, $currentDepartment))
            return response()->json(['requestStatus' => false, 'message' => trans('Server Internal Error 500')]);

        return response()->json(['requestStatus' => true, 'message' => trans('Data Updated Successfully')]);
    }

    public function edit($id)
    {
        $department = Department::find($id);
        return view('Backend.departments.edit', compact('department'));
    }

    public function update(EditDepartmentRequest $request, $id)
    {
        $departmentData = $request->all();
        $currentDepartment = Department::find($id);
        
        if (Department::updateDepartment($departmentData, $currentDepartment)) {

            if (request()->ajax()) {
                return response()->json(['requestStatus' => true, 'message' => __('Data Updated Successfully')]);
            }else{
                request()->session()->flash('status','success');
                request()->session()->flash('message',__('Data Updated Successfully'));
            }
        } else {
            if (request()->ajax()) {
                return response()->json(['requestStatus' => false, 'message' => __('Server Internal Error 500')]);
            } else {
                 request()->session()->flash('status','danger');
                 request()->session()->flash('message',__('Server Internal Error 500'));
            }
        }

        return redirect()->route('departments.edit', ['id' => $currentDepartment->id]);
    }

    public function DeleteDepartment(Request $request)
    {
        $department = Department::find($request->id);

        if(!$department) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, Department is not exists !!']);

        try
        {            
            $department->delete();
            
            return response()->json(['deleteStatus' => true, 'message' => 'Department Deleted Successfully']);
        }
        catch (Exception $e){ return response()->json(['deleteStatus' => false,'error' => 'Server Internal Error 500']); }

        return redirect()->route('departments.index');
    }

    public function RestoreDepartment(Request $request)
    {
        $restore = DB::table('departments')->where('id', $request->id)->where('deleted_at', '!=', null)->update(['deleted_at' => null]);
        
        $response['requestStatus']  = ($restore) ? true : false;
        $response['message']        = ($restore) ? 'Department Restored Successfully' : 'Server Internal Error 500';

        return response()->json($response);
    }

    public function RemoveDepartment(Request $request)
    {
        $remove = DB::table('departments')->where('id', $request->id)->where('deleted_at', '!=', null);

        if (isset($remove->image) && $remove->image != null) File::delete('uploaded/departments/'.$remove->image);

        $remove->delete();
        
        $response['requestStatus']  = ($remove) ? true : false;

        return response()->json($response);
    }
}
