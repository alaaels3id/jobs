<div class="form-group">
	@include('includes.backend.inputs', ['errors' => $errors, 'type' => 'text', 'name' => 'name', 'style' => 'form-control form-data','col'=>'6'])
	@include('includes.backend.inputs', ['errors' => $errors, 'type' => 'text', 'name' => 'description', 'style' => 'form-control form-data','col'=>'6'])
</div>

<div class="form-group">
	@include('includes.backend.inputs', ['errors' => $errors, 'type' => 'number', 'name' => 'salary', 'style' => 'form-control form-data','col'=>'6'])
</div>