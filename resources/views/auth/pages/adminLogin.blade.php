@extends('auth.layouts.auth')

@section('title', 'Admin login')

@section('content')

    <div class="login-box-body">
        <p class="login-box-msg">Sign in as Admin</p>

        @if (Session::has('err'))
            <div class="alert alert-danger" role="alert">
                <strong>Error: </strong> {{ Session::get('err') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        {!! Form::open(['action' => 'Auth\Admin\AdminLoginController@adminLogin', 'method' => 'POST']) !!}

            <div class="form-group {{ $errors->first('email' , 'has-error') }} has-feedback">

                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}

                {!! Html::tag('span', '', ['class' => 'glyphicon glyphicon-envelope form-control-feedback']) !!}

                @if ($errors->has('email'))

                    <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>

                @endif

            </div>
           
            <div class="form-group {{ $errors->first('password' , 'has-error') }} has-feedback">

                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}

                {!! Html::tag('span', '', ['class' => 'glyphicon glyphicon-lock form-control-feedback']) !!}

                @if ($errors->has('password'))

                    <span class="invalid-feedback"><strong>{{ $errors->first('password') }}</strong></span>

                @endif

            </div>

            <div class="row">
                <!-- /.col -->
                <div class="col-xs-12">
                    {{ Form::submit('Sign In', ['class' => 'btn btn-primary btn-block btn-flat']) }}
                </div>
                <!-- /.col -->
            </div>
        {!! Form::close() !!}
        <br>
        <!-- /.social-auth-links -->
        <a href="{{ route('password.request') }}">I forgot my password</a><br>
    </div>

@stop
   
