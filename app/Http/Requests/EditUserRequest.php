<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class EditUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $user  = User::find($this->user);
        $rules = [
            'city_id'    => 'required|numeric|exists:cities,id',
            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'address'    => 'required|string',
            'phone'      => 'required|numeric',
            'image'      => 'nullable|image|mimes:jpg,png,jpeg',
            'email'      => 'required|email|unique:users,email,'.$user->id,
        ];

        return $rules;
    }
}
